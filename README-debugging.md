Maneage: debugging maneage
==========================

Copyright (C) 2024 Boud Roukema\
See the end of the file for license conditions.





Logging
-------

Consider installing and learning "screen" or "tmux", so that you have
a terminal that can be freely detached and re-attached to your session.

Logging your "configure" and "make" steps is essential to checking and
debugging your use of Maneage. If your temporary build directory is
"/my/path/to/a/build/dir/", then you can use a bash command line such as:
```shell
  $ ./project configure --build-dir=/my/path/to/build/dir/ < /dev/null \
     |& tee configure.log.1
```
where "configure.log.1" is the log file to be created, or
```shell
  $ ./project configure --build-dir=/my/path/to/build/dir/ < /dev/null \
     &> tee configure.log.1
```
(More on shell redirection:
 https://en.wikipedia.org/wiki/Redirection_%28computing%29 )

For a repeat attempt at configuring, you could then do
```shell
  $ ./project configure --existing-conf \
     |& tee configure.log.2
```
where "configure.log.2" is the next log file to be created.

Similarly for the "make" step,
```shell
  $ ./project make
     |& tee make.log.1
```
where the log file will be "make.log.1".





Analysing log files
===================

You should be able to find the key lines of most build errors with
```shell
  $ cat config.log.1 | grep --color -in -E "[^\"]error:|make: \*\*\*.*Error"
```
and then you can explore the logs interactively, e.g. with
```shell
  $ nl -ba config.log.1 | less
```
which will let you search interactively for line numbers and
string expressions with '/' to iteratively search forwards and '?'
to iteratively search backwards.

Before posting any log files publicly for analysis, make sure that
you have redacted any personally identifying information, e.g. using
"sed". Publicly showing your personal directory tree is probably unwise,
so something like
```shell
 sed -e 's;/my/path/to/build/dir;/BUILD;g' config.log.1 > config.log.1.clean.1
```
can help with removing that info.






Copyright information
---------------------
This file is part of Maneage (https://maneage.org).

This file is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This file is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this file.  If not, see <http://www.gnu.org/licenses/>.
