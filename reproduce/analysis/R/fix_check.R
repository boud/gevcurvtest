library(ggplot2)
library(gridExtra)
library(cowplot)

model      <- "./shouldbe"
gevolution <- "./fix"

model_a <- read.table(model)[[2]]
model_T <- read.table(model)[[1]]
fix_a   <- read.table(gevolution)[[2]]
fix_T   <- read.table(gevolution)[[1]]

plot <- ggplot() + theme_bw() +
	geom_line(aes(x = model_T, y = model_a), linetype="solid", size=1) +
	geom_line(aes(x = fix_T, y = fix_a), linetype="dotted", size=1) +
	labs(y = "a") +
	xlim(0,13) +
	theme(text = element_text(size=20),
              legend.position = "none",
              panel.border = element_rect(colour = "black", fill=NA, size=0.8)) +
        theme(axis.title.x = element_blank(),
              axis.text.x = element_blank(),
              axis.ticks.x = element_blank()) +
        theme(plot.margin = unit(c(0.3,0.3,0,0.3), "cm"))

x_interp    <- model_T # points where we need extrapolation
interp_data <- spline(x = fix_T, y = fix_a, method = "fmm", xout = model_T)
y_interp    <- interp_data$y

# Residuals
resids_data <- model_a - y_interp

# Percentages
#resids_data <- (model_a - y_interp) * 100 / model_a 

resids <- ggplot() + theme_bw() +
	  geom_line(aes(x = model_T, y = resids_data)) +
	  labs(x = "cosmological time [Gyr]", y = "difference") + 
	  #labs(x = "cosmological time [Gyr]", y = "% of deviation from the model") +
	  xlim(0,13) +
	  theme(plot.margin = unit(c(0,0.3,0.3,0.3), "cm")) +
	  theme(text = element_text(size=20))

plotAll <- plot_grid(plot, resids, align="v", nrow=2, rel_heights = c(0.75, 0.25))

ps_filename <- "fix_check_R.eps"

setEPS()
postscript(ps_filename)
print(plotAll)
dev.off()

