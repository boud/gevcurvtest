#!/usr/bin/env bash

# GPL 3.0 or later Boud Roukema (2015, 2020)
# WARNING this is a crude hack. This is not a systematic solution - a better
# solution would be to understand .bst files

LANG=C

sed -e 's/{de\([ ~][A-Z]\)/{De\1/g' ${1}_tmp.bib | \
    sed -e 's/{van\([ ~][a-zA-Z]\)/{Van\1/g'  \
    > ${1}_tmpDe.bib
cp -v ${1}_tmpDe.bib ${1}_tmp.bib
