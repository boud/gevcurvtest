#!/usr/bin/env bash

# plot gevolution analyse results
# Copyright (C) 2020 Boud Roukema, Justyna Borkowska GPL-3+

# This script plots gevolution results.
#
# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.

#pr --merge --join-lines --omit-headers \
#    output_nophi/lcdm_background.dat \
#    output_phi_diffOmm/lcdm_background.dat | \

LANG=C
LC_NUMERIC=C

cd ${GEVN_DIR}
PHI_INIT=$(grep PHI_INIT ${GEVN_OUTPARS} | awk -F = '{print $2}')

(cat output_nophi/flrw_background.dat | \
        grep -v "^#" | \
        awk '{print $3,sqrt(1.0-2.0*$5)}'; \
 printf "\n\n"; \
 cat output_phi/flrw_background.dat | \
     grep -v "^#" | \
     awk '{print $3,sqrt(1.0-2.0*$5)}' ; \
 printf "\n"; \
 cat a_expected | awk '{print $1,$3/$1}') \
    > a_ratios_for_graph

MAX_Y=$(cat a_expected | awk '{print $3}' | sort -g|tail -n1)
MIN_Y=$(cat a_expected | awk '{print $3}' | sort -g|head -n1)

# -y ${MIN_Y} ${MAX_Y}

printf 'TEMPORARY HACK ONLY - /usr/bin/graph is from the host system and\n'
printf 'should be not be included in the reproducible package!\n'
printf 'COMMENT THESE OUT!!!\n'

cat a_ratios_for_graph | \
    DISPLAY=:0 /usr/bin/graph -Tps \
           -C -m0 -S 16 -lx -X"a\sbFLRW\eb" -Y"a\sbeff\eb/a\sbFLRW\eb" \
           > gevn_curvtest_$(echo ${PHI_INIT}|tr -d ' '|tr '-' 'm').eps

# Quick hack for experimental fix; this will only be usable if
# --gevolution-experimental-fix is selected in the './project configure' step.
if [ "x${experimental_fix}" = "x1" ]; then
    t0=17.3
    awk -v t0=${t0} '{print $2,$1-($2/t0)^(2.0/3.0)}' a_expected > default
    awk -v t0=${t0}  '{print $2,$3-($2/t0)^(2.0/3.0)}' a_expected > shouldbe
    tail -n +3 output_phi/flrw_background.dat |awk -v t0=${t0}  '{print $7,$3*sqrt(1.0-2.0*$5)-($7/t0)^(2.0/3.0)}' > phi
    tail -n +3 output_fix/flrw_background.dat |awk -v t0=${t0}  '{print $7,$3*sqrt(1.0-2.0*$5)-($7/t0)^(2.0/3.0)}' > fix
    #(cat default; printf "\n"; cat shouldbe; printf "\n"; cat phi ; printf "\n"; cat fix)|

    T_RANGE="0.009 18.0"
    A_RANGE="-0.05 0.01"
    (cat default; printf "\n"; cat shouldbe)> default_and_shouldbe

    DISPLAY=:0 /usr/bin/graph -Tps -C -W 0.0001 -L "default; expected; fix" \
           -x ${T_RANGE} -y ${A_RANGE} default_and_shouldbe \
           --reposition 0.0 0.0 1.0 --blankout 0.0 \
           -m0 -S 2 -x ${T_RANGE} -y ${A_RANGE} phi \
           --reposition 0.0 0.0 1.0 --blankout 0.0 \
           -m0 -S 4 -x ${T_RANGE} -y ${A_RANGE} fix \
           > fix.eps

    Rscript ${RDIR}/fix_check.R
fi
