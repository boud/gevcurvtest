#!/bin/bash

# Monitor resources used by a Maneage astronomy paper pipeline
# Copyright (C) 2020-2021 Marius Peper, Boud Roukema, GPL-3+

# This script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.


# 2022-07-01 This script is a project-dependent *optional* tool, and should
# work for the project calculation stage ('analysis'), for at least some
# projects. It does not currently measure full RAM usage by 'maneage'
# processes during the configure stage: a little more work on the script
# would be needed to do that.
# 
# 2020-09-16 This script is currently intended to be run by hand, not by Maneage.
#
# If you choose to use 'free' or 'ps', then you will need the package typically called
# 'procps' in Debian derivative systems.
#
# Conventions for the 'ps' command vary strongly between different Unix-like
# systems. Do not expect this script to work for your system without modification!


FINAL_PDF=gevcurvtest.pdf
rm -f ${FINAL_PDF}
user=$(whoami)
time0=$(date +"%Y-%m-%d.%H.%M.%S")

pid_project=$(ps -u${user} -o pid,args | egrep "\./project.*(make|configure)" |grep -v grep| awk '{print $1}')
printf "pid_project=${pid_project}\n"

while [ ! -f ${FINAL_PDF} ]; do
     time=$(date +"%Y-%m-%d.%H.%M.%S")
     # See if there are any mpirun processes launched by ./project:
     mpi_ps_line=$(ps -u${user} -o pid,ppid,pgid,rss,args| grep mpirun|grep -v grep|head -n1)
     if [ "x${mpi_ps_line}" != "x" ]; then
         #printf "mpi_ps_line=${mpi_ps_line}\n"
         pid_mpi=$(echo "${mpi_ps_line}" | awk '{print $1}')
     else
         # use the project pid as the default
         pid_mpi=${pid_project}
     fi
     #printf "pid_project=${pid_project}; pid_mpi=${pid_mpi};\n"
     RAM_kb=$(ps -u${user} -o pid,ppid,pgid,rss | \
                     awk -v pid_p=${pid_project} -v pid_m=${pid_mpi} \
                         '{if($3==pid_p || $2==pid_m)mem+=$4} END {print mem}')
     #printf "${RAM_kb}\n"
     printf "${time}  ${RAM_kb}\n" >> maneage-astro-RAM-${time0}.log
     sleep 10
 done
