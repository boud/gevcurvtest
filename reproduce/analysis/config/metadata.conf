# Project meta-data that will be used in a project's output datasets and
# final paper. Please set the values here and use them in your analysis or
# paper, don't repeat them.
#
# These variables are used in 'reproduce/analysis/make/initialize.mk': 1)
# to create a Make variable called 'print-general-metadata'. You can simply
# print this variable's value in any plain-text output.
#
# Why add a Copyright for the data? people need to know if they can "use"
# the dataset (i.e., modify it), or possibly re-distribute it and their
# derived products. They also need to know how they can contact the creator
# of the datset (who is usually also the copyright owner). So take this
# seriously and add your name and email-address (or the name of the person
# and email of the person who was in charge of that part of the project),
# and the copyright license name and standard link to the fully copyright
# license.
#
# Copyright (C) 2020-2023 Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Copyright (C) 2021-2024 Boud Roukema
# Copyright (C) 2022-2024 Justyna Borkowska
#
# Copying and distribution of this file, with or without modification, are
# permitted in any medium without royalty provided the copyright notice and
# this notice are preserved.  This file is offered as-is, without any
# warranty.

# Project information
metadata-title = Does relativistic cosmology software handle emergent volume evolution?

# DOIs and identifiers (don't include fixed URL prefixes like
# 'https://doi.org/' or 'https://arxiv.org/abs'), they will be added
# automatically where necessary.
metadata-arxiv = 2112.14174
metadata-zenodo-id-number = 6794222
metadata-zenodo-id = zenodo.$(metadata-zenodo-id-number)
metadata-doi-zenodo = 10.5281/$(metadata-zenodo-id)
metadata-zenodo-url-base = https://zenodo.org/record/$(metadata-zenodo-id-number)
metadata-doi-journal =
metadata-doi = $(metadata-doi-journal)
metadata-git-repository = https://codeberg.org/boud/gevcurvtest
# The SWH ID for a 'rev:' type URI should be the full git commit hash of
# the version of the package considered to be submittable. Warning:
# archiving on request requires a few hours or so before becoming live.
metadata-swh-id = 5a062b564f33ad137dd244b86ee5b758bc26b808
metadata-git-repository-archived = https://archive.softwareheritage.org/swh:1:rev:$(metadata-swh-id)\%3Borigin=$(metadata-git-repository)

# Project-specific for Gevcurvtest
# https://archive.softwareheritage.org/swh:1:cnt:dae4e6de5399a061ab4df01ea51f4757fd7e293a;origin=https://codeberg.org/boud/elaphrocentre.git;visit=swh:1:snp:54f00113661ea30c800b406eee55ea7a7ea35279;anchor=swh:1:rev:a029edd32d5cd41dbdac145189d9b1a08421114e;path=/reproduce/analysis/bash/verify-parameter-statistically.sh
metadata-gev-patch-file-content-ID = 043550246bb1c9c26664a0dbebedb334693033b1
metadata-gev-patch-file-snapshot-ID = bab903fdadb2439d1555d1600c9c5b303970cbb1
metadata-gev-patch-file-revision-ID = 56849c3e096ba77ed6083c0c730ea4498d04b67d
metadata-gev-patch-file-path = /reproduce/software/patches/20210915\%5Fphi\%5Fsource.patch
metadata-gev-patch-file-anchor = swh:1:cnt:$(metadata-gev-patch-file-content-ID)
metadata-gev-patch-file-full-URL = https://archive.softwareheritage.org/swh:1:cnt:$(metadata-gev-patch-file-content-ID)\%3Borigin=$(metadata-git-repository)\%3Bvisit=swh:1:snp:$(metadata-gev-patch-file-snapshot-ID)\%3Banchor=swh:1:rev:$(metadata-gev-patch-file-revision-ID)\%3Bpath=$(metadata-gev-patch-file-path)

metadata-gev-patch-file-swh-href-base = \href{$(metadata-gev-patch-file-full-URL)}{https://archive.softwareheritage.org/}
metadata-gev-patch-file-swh-href-full = \href{$(metadata-gev-patch-file-full-URL)}{$(metadata-gev-patch-file-anchor)}


# Override for development phases of the project.
# This can be used instead of metadata-zenodo-url-base in initialize.mk
# for creating initialize.tex if [ "x${enable_dev_override}" = "xTrue" ]
# in *.conf files. Copying of files to this local URL is *not*
# done automatically.
metadata-zenodo-url-base-dev-override = file:///tmp/zenodo.$(metadata-zenodo-id-number)

# DATA Copyright owner and license information.
metadata-copyright-owner = Justyna Borkowska (jborkowska2203 gmail.com); Boud Roukema (boud astro.uni.torun.pl)
metadata-copyright = Creative Commons Public Domain (CC0)
metadata-copyright-url = https://creativecommons.org/publicdomain/zero/1.0/
