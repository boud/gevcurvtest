## Analyse and plot the simulations
#iproglibdir = $(installdir)/version-info/proglib

eps_file_inhomog_base = inhomog_curvtest_
eps_file_gevn_base = gevn_curvtest_

# Tables
output_tables_dat = $(data-publish-dir)/flrw_ref_eff_constants.dat \
       $(data-publish-dir)/inhomog_scale_factors_EdS.dat \
       $(data-publish-dir)/inhomog_scale_factors_LCDM.dat \
       $(data-publish-dir)/gevolution_scale_factors_EdS.dat \
       $(data-publish-dir)/gevolution_scale_factors_LCDM.dat \
       $(data-publish-dir)/accuracy_parameters.dat

#output_tables_dat_basename = $(subst $(data-publish-dir)/,,$(output_tables_dat))



$(done_inhomog_plot): $(done_inhomog)
	rm -fv $(inhomog_dir)/inhomog_accuracy
	for Ncroot_inhomog in $(call powers_two_macro,$(Ncroot_inhomog_list)); do
	 for flrw in $(flrw_ref); do
	  for init_perturb_inhomog in $(init_perturb_inhomog_list); do
	    runname=$${flrw}$$(echo $${init_perturb_inhomog}|tr '.' '_')_$${Ncroot_inhomog}

	    INHOMOG_LOG=$$(ls -t $(inhomog_dir)/$${runname}/output_I/ramses.Lbox*log|head -n1)
        # Both LCDM and EdS cases are calculated; only half the total
        # number of output lines are needed:
	    N_LINES=$$(grep a_D_global $${INHOMOG_LOG} | wc -l | awk '{print $$1/2}')
	    if [ "x$${flrw}" = "xLCDM" ]; then
	      # The first set of a_D_global lines of Omega_D_precalc are
	      # for LCDM when INHOM_LCDM_EDS is set at compile time.
	      N_LINES_HEAD=$${N_LINES}
	    elif [ "x$${flrw}" = "xEdS" ]; then
	      # The second set of a_D_global lines of Omega_D_precalc are
	      # for EdS when INHOM_LCDM_EDS is set at compile time.
	      N_LINES_HEAD=$$(echo $${N_LINES} | awk '{print 2*$$1}')
	    fi
        # With a perturbation
	    (grep a_D_global $${INHOMOG_LOG} | head -n $${N_LINES_HEAD} | \
	       tail -n $${N_LINES} | awk '{print $$2,$$3,$$5}') \
	       > $(inhomog_dir)/$${runname}/output_I_t_aref_aeff
	    INHOMOG_LOG=$$(ls -t $(inhomog_dir)/$${runname}/output_noI/ramses.Lbox*log|head -n1)
        # With NO perturbation
	    (grep a_D_global $${INHOMOG_LOG} | head -n $${N_LINES_HEAD} | \
	       tail -n $${N_LINES} | awk '{print $$2,$$3,$$5}') \
	       > $(inhomog_dir)/$${runname}/output_noI_t_aref_aeff
	    INHOMOG_DIR=$(inhomog_dir)/$${runname} \
	      INHOMOG_OUTPARS=$(inhomog_dir)/$${runname}/outputs.$${flrw} \
	      Rscript $(rdir)/plot_inhomog.R | tee log.plot_inhomog.R_$${runname}
	    #for file in $(eps_files_inhomog); do
	    eps_file_R=$(eps_file_inhomog_base)$$(echo $${init_perturb_inhomog}|tr '-' 'm').eps
	    eps_file_unique=$(eps_file_inhomog_base)$$(echo $${runname}|tr '-' 'm').eps
	    cp -pv $(badir)/inhomog/$${runname}/$${eps_file_R} $(texbdir)/$${eps_file_unique}
	    #done
        #
        # Calculate final deviation between the expectation and the result
            # Assume that the R script only calculates through to the current epoch in the reference model.
	    inhomog_accuracy_fraction=$$(grep "accuracy = " log.plot_inhomog.R_$${runname} |tail -n1 |awk -F '=' '{printf "%.3g", $$2}')
	    inhomog_accuracy_percent=$$(grep "accuracy = " log.plot_inhomog.R_$${runname} |tail -n1 |awk -F '=' '{printf "%.4f", 100.0*$$2}')
	    inhomog_expected_aratio=$$(grep "expected_aratio = " log.plot_inhomog.R_$${runname} |tail -n1 |awk -F '=' '{if($$2<1) printf "%#.5g", $$2; else printf "%#.6g", $$2}')
	    inhomog_perturb_aratio=$$(grep "perturb_aratio = " log.plot_inhomog.R_$${runname} |tail -n1 |awk -F '=' '{if($$2<1) printf "%#.5g", $$2; else printf "%#.6g", $$2}')
            # Substitute first underscore with 'd'; others with 'u';
            #            minuses with 'm'; digits 0 or 1 or 3 with 'z' or 'o' or 't'.
            # TODO: handle other digits?
	    runname_alpha=$$(echo $${runname}| \
	       sed -e 's/_/d/' -e 's/_/u/g' -e 's/-/m/g' -e 's/0/z/g' -e 's/1/o/g' -e 's/3/t/g')
	    printf "inhomog_accuracy_fraction_$${runname_alpha} = $${inhomog_accuracy_fraction}\n" >> $(inhomog_dir)/inhomog_accuracy
	    printf "inhomog_accuracy_percent_$${runname_alpha} = $${inhomog_accuracy_percent}\n" >> $(inhomog_dir)/inhomog_accuracy
	    printf "inhomog_expected_aratio_$${runname_alpha} = $${inhomog_expected_aratio}\n" >> $(inhomog_dir)/inhomog_accuracy
	    printf "inhomog_perturb_aratio_$${runname_alpha} = $${inhomog_perturb_aratio}\n" >> $(inhomog_dir)/inhomog_accuracy
	  done
	 done
	done
	touch $(done_inhomog_plot)

        #cat tmp_inhomog | /usr/bin/graph -Tps -y 0.995 1.005 > inhomog_plot.eps

$(done_gevolution_plot): $(done_gevolution)
        # Temporary hack: $(bashdir)/plot-gevolution is only a quick hack; comment it out when the plot with R is done.
        # if (grep "experimental-fix" $$(ls -t $(iproglibdir)/gevolution*|head -n1)); then
        #   experimental_fix=1; \
        # else \
        #   experimental_fix=0; \
        # fi
        # for flrw in $(flrw_ref); do
        #   for init_perturb_gevn in $(init_perturb_gevn_list); do
        #     runname=$${flrw}$$(echo $${init_perturb_gevn}|tr '.' '_')
        #     RDIR=$(curdir)/$(rdir) \
        #     SHARE_DIR=$(isharedir)/gevolution \
        #       GEVN_DIR=$(badir)/gevolution/$${runname} \
        #       GEVN_OUTPARS=$${GEVN_DIR}/outputs.$${flrw} \
        #       experimental_fix=$${experimental_fix} \
        #       $(bashdir)/plot-gevolution
        #   done
        # done

	set -o pipefail
        # Warning: Pipefail requires using "sed '1!d'" instead of "head -n1",
        # because (ironically) "head" is too efficient. The problem is that
        # "head -n1" stops running after it has read a line, so the command
        # giving its stdout to the pipe that was being read by "head" no
        # longer has anywhere to send its output. So that particular program
        # fails.
	rm -fv $(badir)/gevolution/gevolution_accuracy
	for Ncroot_gevolution in $(call powers_two_macro,$(Ncroot_gevolution_list)); do
	 for flrw in $(flrw_ref); do
	  printf "Gevolution figures: will handle flrw_ref=%s ...\n" $${flrw}
	  for init_perturb_gevn in $(init_perturb_gevn_list); do
	    printf "Gevolution figures: will handle flrw_ref=%s and init_perturb_gevn=%f ...\n" $${flrw} $${init_perturb_gevn}
	    runname=$${flrw}$$(echo $${init_perturb_gevn}|tr '.' '_')_$${Ncroot_gevolution}
	      GEVN_DIR=$(badir)/gevolution/$${runname} \
	        GEVN_OUTPARS=$(badir)/gevolution/$${runname}/outputs.$${flrw} \
	        Rscript $(rdir)/plot_gevolution.R | tee log.plot_gevn.R_$${runname}
	      printf "Line after running plot_gevolution.R once.\n"
	      eps_file_R=$(eps_file_gevn_base)$$(echo $${init_perturb_gevn}|tr '-' 'm').eps
	      eps_file_unique=$(eps_file_gevn_base)$$(echo $${runname}|tr '-' 'm').eps
	      md5sum log.plot_gevn.R_$${runname}
	      cp -pv $(badir)/gevolution/$${runname}/$${eps_file_R} $(texbdir)/$${eps_file_unique}
	      # Calculate biggest deviation between the expectation and the result (absolute value)
	      gevolution_accuracy_fraction_max=$$(grep "accuracy = " log.plot_gevn.R_$${runname} | sed -e 's/-//g' | tail -n +2 | sort -rg -k3,3 | sed '1!d' | awk -F '=' '{printf "%.3g", $$2}')
	      gevolution_accuracy_percent_max=$$(grep "accuracy = " log.plot_gevn.R_$${runname} | sed -e 's/-//g' | tail -n +2 | sort -rg -k3,3 | sed '1!d' | awk -F '=' '{printf "%.4g", 100.0*$$2}')
              # Calculate final deviation between the expectation and the result
	      gevolution_accuracy_fraction_final=$$(grep "accuracy = " log.plot_gevn.R_$${runname} |tail -n1 |awk -F '=' '{printf "%.3g", $$2}')
	      gevolution_accuracy_percent_final=$$(grep "accuracy = " log.plot_gevn.R_$${runname} |tail -n1 |awk -F '=' '{printf "%.3g", 100.0*$$2}')
	      gevolution_expected_aratio_final=$$(grep "expected_aratio = " log.plot_gevn.R_$${runname} |tail -n1 |awk -F '=' '{if($$2<1) printf "%#.5g", $$2; else printf "%#.6g", $$2}')
	      gevolution_phi_aratio_final=$$(grep "phi_aratio = " log.plot_gevn.R_$${runname} |tail -n1 |awk -F '=' '{if($$2<1) printf "%#.5g", $$2; else printf "%#.6g", $$2}')
	      runname_alpha=$$(echo $${runname}| \
	         sed -e 's/_/d/' -e 's/_/u/g' -e 's/-/m/g' -e 's/0/z/g' -e 's/1/o/g' -e 's/3/t/g')
	      printf "gevolution_accuracy_percent_max_$${runname_alpha} = $${gevolution_accuracy_percent_max}\n" >> $(badir)/gevolution/gevolution_accuracy
	      printf "gevolution_accuracy_percent_final_$${runname_alpha} = $${gevolution_accuracy_percent_final}\n" >> $(badir)/gevolution/gevolution_accuracy
	      printf "gevolution_expected_aratio_final_$${runname_alpha} = $${gevolution_expected_aratio_final}\n" >> $(badir)/gevolution/gevolution_accuracy
	      printf "gevolution_phi_aratio_final_$${runname_alpha} = $${gevolution_phi_aratio_final}\n" >> $(badir)/gevolution/gevolution_accuracy
	  done
	 done
	done
	touch $(done_gevolution_plot)

$(data-publish-dir)/flrw_ref_eff_constants.dat: $(mtexdir)/analyse-plot.tex

$(mtexdir)/analyse-plot.tex: $(done_gevolution_plot) $(done_inhomog_plot) \
                $(data-publish-dir)/inhomog_scale_factors_LCDM.dat \
                $(data-publish-dir)/gevolution_scale_factors_LCDM.dat \
                $(data-publish-dir)/accuracy_parameters.dat \
                | $(data-publish-dir)
	printf "%% Automatically produced file.\n" > $@

        # Accuracies of inhomog and gevolution calculations: LaTeX macros.
	for param_name in $$(egrep "inhomog_(accuracy_percent)" $(inhomog_dir)/inhomog_accuracy| \
	       tr '\n' '@' |tr -d ' '|tr '@' ' '); do
	   param_name_tex=$$(echo $${param_name}| awk -F '=' '{print $$1}' | sed -e 's/[^_-]*/\u&/g' |tr -d '_'|tr -d '-')
	   param_value=$$(echo $${param_name}| awk -F '=' '{print $$2}') # format decided when creating the file 'inhomog_accuracy'
	   printf "\\\\newcommand{\\\\$${param_name_tex}value}{$${param_value}}\n" >> $@
	done
	final_scale_factor="" # initialise the string of final scale factors
	for param_name in $$(egrep "inhomog_(expected|perturb)_aratio" $(inhomog_dir)/inhomog_accuracy| \
	       tr '\n' '@' |tr -d ' '|tr '@' ' '); do
	   param_name_tex=$$(echo $${param_name}| awk -F '=' '{print $$1}' | sed -e 's/[^_-]*/\u&/g' |tr -d '_'|tr -d '-')
	   param_value=$$(echo $${param_name}| awk -F '=' '{print $$2}') # format decided when creating the file 'inhomog_accuracy'
	   printf "\\\\newcommand{\\\\$${param_name_tex}value}{$${param_value}}\n" >> $@
	   final_scale_factor="$${final_scale_factor} $${param_value}" # add to the string of final scale factors
	done

	for param_name in $$(egrep "gevolution_(accuracy_percent)" $(badir)/gevolution/gevolution_accuracy| \
	       tr '\n' '@' |tr -d ' '|tr '@' ' '); do
	   param_name_tex=$$(echo $${param_name}| awk -F '=' '{print $$1}' | sed -e 's/[^_-]*/\u&/g' |tr -d '_'|tr -d '-')
	   param_value=$$(echo $${param_name}| awk -F '=' '{printf "%.03f", $$2}') # 1 decimal float
	   printf "\\\\newcommand{\\\\$${param_name_tex}value}{$${param_value}}\n" >> $@
	done
	for param_name in $$(egrep "gevolution_(expected|phi)_aratio" $(badir)/gevolution/gevolution_accuracy| \
	       tr '\n' '@' |tr -d ' '|tr '@' ' '); do
	   param_name_tex=$$(echo $${param_name}| awk -F '=' '{print $$1}' | sed -e 's/[^_-]*/\u&/g' |tr -d '_'|tr -d '-')
	   param_value=$$(echo $${param_name}| awk -F '=' '{printf "%.5f", $$2}') # 1 decimal float
	   printf "\\\\newcommand{\\\\$${param_name_tex}value}{$${param_value}}\n" >> $@
	   final_scale_factor="$${final_scale_factor} $${param_value}" # add to the string of final scale factors
	done

        # Reference and effective model FLRW constants - LaTeX macros and table
	datafile=$(data-publish-dir)/flrw_ref_eff_constants.dat
	$(call print-general-metadata, $${datafile}, new) # start the table
        # Parameter list
	param_list_table="A_INIT_REF OMEGA_M0_REF OMEGA_LAM0_REF OMEGA_K0_REF H0_REF OMEGA_M0_EFF OMEGA_LAM0_EFF OMEGA_K0_EFF A_DOT_0_EFF"

        # Write parameter headers
	printf "#\n" >> $${datafile}
	printf "# Col  1: FLRW model\n" >> $${datafile}
	printf "# Col  2: initial first invariant I (inhomog) or initial Phi (gevolution)\n" >> $${datafile}
	printf "# Col  3: a^r_i -- reference model initial scale factor\n" >> $${datafile}
	printf "# Col  4: Omega_{m0}^{r} -- reference model current-epoch FLRW constants\n" >> $${datafile}
	printf "# Col  5: Omega_{Lam0}^{r}\n" >> $${datafile}
	printf "# Col  6: Omega_{k0}^{r}\n" >> $${datafile}
	printf "# Col  7: H_{m0}^{r}\n" >> $${datafile}
	printf "# Col  8: Omega_{m0}^{e} -- effective model current-epoch FLRW constants\n" >> $${datafile}
	printf "# Col  9: Omega_{Lam0}^{e}\n" >> $${datafile}
	printf "# Col 10: Omega_{k0}^{e}\n" >> $${datafile}
	printf "# Col 11: H_{m0}^{e}\n" >> $${datafile}
	printf "# Col 12: a^e_{FLRW}(a^r=1) - effective model current-epoch scale factor\n" >> $${datafile}
	printf "# Col 13: a^e(a^r=1) - current-epoch effective scale factor from simulation\n" >> $${datafile}
	printf "#\n" >> $${datafile}

	for Ncroot_inhomog in $(call powers_two_macro,$(lastword $(Ncroot_inhomog_list))); do
	 for flrw in $(flrw_ref); do
	  for init_perturb_inhomog in $(init_perturb_inhomog_list); do
	    runname=$${flrw}$$(echo $${init_perturb_inhomog}|tr '.' '_')_$${Ncroot_inhomog}
	    runname_prefix=Inhomog_$$(echo $${runname}|sed -e 's/-/m/g')
	    param_list=$$(cat $(inhomog_dir)/$${runname}/outputs.$${flrw}| \
	                   awk '{print $$1}' | tr '\n' ' '| tr -s ' ')
	    $(call create_tex_macro,
	           $(inhomog_dir)/$${runname}/outputs.$${flrw}, \
	           $${param_list}, \
	           float.3, \
	           $${runname_prefix})
	    printf "%13s" inhomog_$${flrw} >> $${datafile}
	    printf "%8.3f" $${init_perturb_inhomog} >> $${datafile}
	    for want_param in $${param_list_table}; do
	      for possible_param in $${param_list}; do
	        if [ x$$(echo "$${possible_param}"|awk -F '=' '{print $$1}') = x"$${want_param}" ] ; then
	           s=$$(echo $${possible_param} | awk -F '=' '{print $$2}')
	           printf "%10.5f" "$${s}" >> $${datafile}
	        fi
	      done
	    done
	    echo "$${final_scale_factor}" | awk '{printf "%10.5f %10.5f", $$1,$$2}' >> $${datafile}
            # Remove the two values just printed out from 'final_scale_factor', since they are no longer needed.
	    final_scale_factor=$$(echo $${final_scale_factor} | sed -e 's/^\([0-9.-]*\) \([0-9.-]*\)//')
	    printf "\n" >> $${datafile}
	  done
	 done
	done


        # 2022-06-20 Post-referee revision: store A_INIT_REF as macros for
        # the different resolutions to explain effect of resolution.
	i_croot=0
	Ncroot_letters=($(call powers_two_macro,$(Ncroot_inhomog_list)))
	for Ncroot_digits in $(Ncroot_inhomog_list); do
	 for flrw in $(flrw_ref); do
	  for init_perturb_inhomog in $(init_perturb_inhomog_list); do
	    runname=$${flrw}$$(echo $${init_perturb_inhomog}|tr '.' '_')_$${Ncroot_letters[$${i_croot}]}
	    runname_prefix=Inhomog_$$(echo $${runname}|sed -e 's/-/m/g')
	    $(call create_tex_macro,
	           $(inhomog_dir)/$${runname}/outputs.$${flrw}, \
	           A_INIT_REF, \
	           float.3, \
	           $${runname_prefix}Resolution)
	  done
	 done
	 i_croot=$$(($${i_croot}+1))
	done


	for Ncroot_gevolution in $(call powers_two_macro,$(lastword $(Ncroot_gevolution_list))); do
	 for flrw in $(flrw_ref); do
	  for init_perturb_gevn in $(init_perturb_gevn_list); do
	    runname=$${flrw}$$(echo $${init_perturb_gevn}|tr '.' '_')_$${Ncroot_gevolution}
	    runname_prefix=Gevolution_$$(echo $${runname}|sed -e 's/-/m/g')
	    param_list=$$(cat $(badir)/gevolution/$${runname}/outputs.$${flrw}| \
	                   awk '{print $$1}' | tr '\n' ' '| tr -s ' ')
	    $(call create_tex_macro,
	           $(badir)/gevolution/$${runname}/outputs.$${flrw}, \
	           $${param_list}, \
	           float.3, \
	           $${runname_prefix})
	    printf "%13s" gevolut_$${flrw} >> $${datafile}
	    printf "%8.3f" $${init_perturb_gevn} >> $${datafile}
	    for want_param in $${param_list_table}; do
	      for possible_param in $${param_list}; do
	        if [ x$$(echo "$${possible_param}"|awk -F '=' '{print $$1}') = x"$${want_param}" ] ; then
	           s=$$(echo $${possible_param} | awk -F '=' '{print $$2}')
	           printf "%10.5f" "$${s}" >> $${datafile}
	        fi
	      done
	    done
	    echo "$${final_scale_factor}" | awk '{printf "%10.5f %10.5f", $$1,$$2}' >> $${datafile}
            # Remove the two values just printed out from 'final_scale_factor', since they are no longer needed.
	    final_scale_factor=$$(echo $${final_scale_factor} | sed -e 's/^\([0-9.-]*\) \([0-9.-]*\)//')
	    printf "\n" >> $${datafile}
	  done
	 done
	done 


$(data-publish-dir)/inhomog_scale_factors_LCDM.dat: $(data-publish-dir)/inhomog_scale_factors_EdS.dat

$(data-publish-dir)/inhomog_scale_factors_EdS.dat: $(done_inhomog_plot) \
                | $(data-publish-dir)
        # Inhomog figures to plain-text tables
	for Ncroot_inhomog in $(call powers_two_macro,$(lastword $(Ncroot_inhomog_list))); do
	 for flrw in $(flrw_ref); do
	  datafile=$(data-publish-dir)/inhomog_scale_factors_$${flrw}.dat
	  $(call print-general-metadata, $${datafile}, new) # start the table

          # Write parameter headers
	  printf "#\n" >> $${datafile}
	  column=0
	  for init_perturb_inhomog in $(init_perturb_inhomog_list); do
	    runname=$${flrw}$$(echo $${init_perturb_inhomog}|tr '.' '_')_$${Ncroot_inhomog}
	    printf "# $${flrw}; initial I inv = $${init_perturb_inhomog}:\n" >> $${datafile}
	    column=$$(($${column}+1))
	    printf "# Col %2d: a^r = reference model scale factor\n" $${column} >> $${datafile}
	    column=$$(($${column}+1))
	    printf "# Col %2d: a^e_{FLRW} =  expected FLRW scale factor\n" $${column} >> $${datafile}
	    column=$$(($${column}+1))
	    printf "# Col %2d: a^e = numerically measured effective scale factor\n" $${column} >> $${datafile}
	    column=$$(($${column}+1))
	    printf "# Col %2d: epsilon = accuracy (0 = perfect, +1 = +100%% wrong)\n" $${column} >> $${datafile}
	  done

	  rm -fv tmp.log.inhomog.*
	  for init_perturb_inhomog in $(init_perturb_inhomog_list); do
	    runname=$${flrw}$$(echo $${init_perturb_inhomog}|tr '.' '_')_$${Ncroot_inhomog}
	    grep "inhomog_plot:" log.plot_inhomog.R_$${runname} | awk '{print $$2,$$3,$$4,$$5}' > tmp.log.inhomog.$${runname}
	  done
	  pr --merge --omit-header --join-lines tmp.log.inhomog.* >> $${datafile}
	 done # for flrw in ...
	done # 	for Ncroot_inhomog in $(lastword $(Ncroot_inhomog_list)); do


$(data-publish-dir)/gevolution_scale_factors_LCDM.dat: $(data-publish-dir)/gevolution_scale_factors_EdS.dat

$(data-publish-dir)/gevolution_scale_factors_EdS.dat: $(done_gevolution_plot) \
                | $(data-publish-dir)
        # Gevolution figures to plain-text tables
	for Ncroot_letters in $(call powers_two_macro,$(lastword $(Ncroot_gevolution_list))); do
	 for flrw in $(flrw_ref); do
	  datafile=$(data-publish-dir)/gevolution_scale_factors_$${flrw}.dat
	  $(call print-general-metadata, $${datafile}, new) # start the table

          # Write parameter headers
	  printf "#\n" >> $${datafile}
	  column=0
	  for init_perturb_gevn in $(init_perturb_gevn_list); do
	    runname=$${flrw}$$(echo $${init_perturb_gevn}|tr '.' '_')_$${Ncroot_letters}
	    printf "# $${flrw}; initial Phi = $${init_perturb_gevn}:\n" >> $${datafile}
	    column=$$(($${column}+1))
	    printf "# Col %2d: a^r = reference model scale factor\n" $${column} >> $${datafile}
	    column=$$(($${column}+1))
	    printf "# Col %2d: a^e_{FLRW} =  expected FLRW scale factor\n" $${column} >> $${datafile}
	    column=$$(($${column}+1))
	    printf "# Col %2d: a^e = numerically measured effective scale factor\n" $${column} >> $${datafile}
	    column=$$(($${column}+1))
	    printf "# Col %2d: epsilon = accuracy (0 = perfect, +1 = +100%% wrong)\n" $${column} >> $${datafile}
	  done

	  rm -fv tmp.log.gevn.*
	  for init_perturb_gevn in $(init_perturb_gevn_list); do
	    runname=$${flrw}$$(echo $${init_perturb_gevn}|tr '.' '_')_$${Ncroot_letters}
	    grep "gevn_plot:" log.plot_gevn.R_$${runname} | awk '{print $$2,$$3,$$4,$$5}' > tmp.log.gevn.$${runname}
	  done
	  pr --merge --omit-header --join-lines tmp.log.gevn.* >> $${datafile}
	 done # for flrw in ...
	done # 	for Ncroot_gevolution in $(lastword $(Ncroot_gevolution_list)); do


$(data-publish-dir)/accuracy_parameters.dat: $(data-publish-dir)/inhomog_scale_factors_LCDM.dat \
              $(data-publish-dir)/gevolution_scale_factors_LCDM.dat
	datafile=$(data-publish-dir)/accuracy_parameters.dat
	$(call print-general-metadata, $${datafile}, new) # start the table

        # INHOMOG: Write parameter headers
	printf "#\n" >> $${datafile}
	printf "# Col  1: particle resolution N^(1/3)\n" >> $${datafile}
	i_croot=0
	i_col=1
	Ncroot_letters=($(call powers_two_macro,$(firstword $(Ncroot_inhomog_list))))
	for Ncroot_digits in $(firstword $(Ncroot_inhomog_list)); do
	 for flrw in $(flrw_ref); do
	  for init_perturb_inhomog in $(init_perturb_inhomog_list); do
	    i_col=$$(($${i_col}+1))
	    printf "# Col  %d: accuracy epsilon (as a percentage; for inhomog %s init invariant I = %.04f)\n" $${i_col} $${flrw} $${init_perturb_inhomog} \
	          >> $${datafile}
	  done
	 done
	 i_croot=$$(($${i_croot}+1))
	done

	i_croot=0
	Ncroot_letters=($(call powers_two_macro,$(Ncroot_inhomog_list)))
	for Ncroot_digits in $(Ncroot_inhomog_list); do
	 printf "%4d " $${Ncroot_digits} >> $${datafile}
	 for flrw in $(flrw_ref); do
	  for init_perturb_inhomog in $(init_perturb_inhomog_list); do
	    runname=$${flrw}$$(echo $${init_perturb_inhomog}|tr '.' '_')_$${Ncroot_letters[$${i_croot}]}
	    runname_alpha=$$(echo $${runname}| \
	       sed -e 's/_/d/' -e 's/_/u/g' -e 's/-/m/g' -e 's/0/z/g' -e 's/1/o/g' -e 's/3/t/g')
	    printf "%.04f " $$(grep accuracy_percent_$${runname_alpha} $(inhomog_dir)/inhomog_accuracy | \
	       awk -F '=' '{print $$2}') >> $${datafile}
	  done
	 done
	 printf "\n" >> $${datafile}
	 i_croot=$$(($${i_croot}+1))
	done

        # GEVOLUTION: Write parameter headers
	printf "#\n" >> $${datafile}
	printf "# Col  1: particle resolution N^(1/3)\n" >> $${datafile}
	i_croot=0
	i_col=1
	Ncroot_letters=($(call powers_two_macro,$(firstword $(Ncroot_gevolution_list))))
	for Ncroot_digits in $(firstword $(Ncroot_gevolution_list)); do
	 for flrw in $(flrw_ref); do
	  for init_perturb_gevn in $(init_perturb_gevn_list); do
	    i_col=$$(($${i_col}+1))
	    printf "# Col  %d: accuracy epsilon (as a percentage; for gevolution %s init Phi = %.04f)\n" $${i_col} $${flrw} $${init_perturb_gevn} \
	          >> $${datafile}
	  done
	 done
	 i_croot=$$(($${i_croot}+1))
	done

	i_croot=0
	Ncroot_letters=($(call powers_two_macro,$(Ncroot_gevolution_list)))
	for Ncroot_digits in $(Ncroot_gevolution_list); do
	 printf "%4d " $${Ncroot_digits} >> $${datafile}
	 for flrw in $(flrw_ref); do
	  for init_perturb_gevn in $(init_perturb_gevn_list); do
	    runname=$${flrw}$$(echo $${init_perturb_gevn}|tr '.' '_')_$${Ncroot_letters[$${i_croot}]}
	    runname_alpha=$$(echo $${runname}| \
	       sed -e 's/_/d/' -e 's/_/u/g' -e 's/-/m/g' -e 's/0/z/g' -e 's/1/o/g' -e 's/3/t/g')
	    printf "%.05f " $$(grep accuracy_percent_max_$${runname_alpha} $(badir)/gevolution/gevolution_accuracy | \
	       awk -F '=' '{print $$2}') >> $${datafile}
	  done
	 done
	 printf "\n" >> $${datafile}
	 i_croot=$$(($${i_croot}+1))
	done


.PHONY: clean-analyse-plot

clean-analyse-plot:
	rm -f $(done_gevolution_plot) $(done_inhomog_plot)
