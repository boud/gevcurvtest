## Initialise the simulations

## TODO: remove these - apparently they are set by initialize.mk
## mtexdir     = $(texdir)/macros
## pconfdir = reproduce/analysis/config

## Set up macros from parameter values.

ibdir = $(bsdir)/installed/bin
init_dir = $(badir)/n-body-init
ifeq (x$(devmode),x1)
paramsfile = $(pconfdir)/Nbody-sim-params-devmode.conf
else
paramsfile = $(pconfdir)/Nbody-sim-params.conf
endif
example_initcond_output_files = $(foreach s, $(flrw_ref), \
              $(foreach t, $(subst .,_, $(init_perturb_inhomog_list)), \
              $(foreach u, $(Ncroot_inhomog_list), \
              $(init_dir)/$(s)$(t)_$(call powers_two_macro,$(u))/ic_velcz)))
mpgrafic_sanity_checks = $(foreach s, $(flrw_ref), \
              $(foreach t, $(subst .,_, $(init_perturb_inhomog_list)), \
              $(foreach u, $(Ncroot_inhomog_list), \
              $(init_dir)/$(s)$(t)_$(call powers_two_macro,$(u))/mpgrafic-sanity-check)))

#mpgrafic_sanity_checks = $(foreach s, $(flrw_ref), $(init_dir)/$(s)$(init_perturb_inhomog_1)/mpgrafic-sanity-check)


include ${paramsfile}

# aliases
mpgrafic: init-conditions
init-cond: init-conditions

# This is the main rule. It says that if initial conditions are
# wanted, then at least one of the mpgrafic output files has to exist
# and a TeX file with macros with the input parameter values has to
# exist. A simple sanity check (TODO: that should later be moved to verify.mk!)
# is also required to be run.
init-conditions: $(example_initcond_output_files) $(mtexdir)/init-N-body.tex $(mpgrafic_sanity_checks)

# Before running the sanity check, first make sure that the file has been created
$(mpgrafic_sanity_checks) &: $(example_initcond_output_files)
	i_croot=0
	Ncroot_letters=($(call powers_two_macro,$(Ncroot_inhomog_list)))
	for Ncroot_digits in $(Ncroot_inhomog_list); do \
	 for flrw in $(flrw_ref); do
	  for init_perturb_inhomog in $(init_perturb_inhomog_list); do
	    runname=$${flrw}$$(echo $${init_perturb_inhomog}|tr '.' '_')_$${Ncroot_letters[$${i_croot}]}
	    printf "mpgrafic_sanity_checks rule: Ncroot_digits = %d  Ncroot_letters = %s  runname = %s i_croot = %d\n" \
	          $${Ncroot_digits} $${Ncroot_letters[$${i_croot}]} $${runname} $${i_croot}
	    output_file=$(init_dir)/$${runname}/ic_velcz
	    byte_count=$$(wc -c $${output_file}|awk '{print $$1}')
	    expected_byte_count=$$(echo "52" |awk -v N=$${Ncroot_digits} '{print $$1+8*N+4*N*N*N}')
	    if [ "$${byte_count}" -eq "$${expected_byte_count}" ]; then
	         printf "$${output_file} has the expected byte count.\n"
	         touch $(init_dir)/$${runname}/mpgrafic-sanity-check
	    else
	         printf "Warning: $${output_file} is $${byte_count} bytes but $${expected_byte_count} was expected.\n"
	         rm -f $(init_dir)/$${runname}/mpgrafic-sanity-check
	    fi
	  done
	 done
	 i_croot=$$(($${i_croot}+1));
	done

$(example_initcond_output_files) &: $(paramsfile)
        # This section runs mpgrafic using the parameters from
        # $(paramsfile). It will be re-run if $(paramsfile) is
        # updated.
	printf "Will start init-N-body.mk main loop; devmode=%s .\n" $(devmode)
	i_croot=0
	Ncroot_letters=($(call powers_two_macro,$(Ncroot_inhomog_list)))
	for Ncroot_digits in $(Ncroot_inhomog_list); do \
	 for init_perturb_inhomog in $(init_perturb_inhomog_list); do \
	  count=0
	  for flrw in $(flrw_ref); do \
	    runname=$${flrw}$$(echo $${init_perturb_inhomog}|tr '.' '_')_$${Ncroot_letters[$${i_croot}]} \
	    && printf "This is init-N-body.mk starting from " \
	    && $(installdir)/bin/pwd && printf "\n" \
	    && printf "Ncroot_digits = %d  Ncroot_letters = %s  runname = %s i_croot = %d\n" \
	          $${Ncroot_digits} $${Ncroot_letters[$${i_croot}]} $${runname} $${i_croot} \
	    && echo $(init_dir)/$${runname} \
	    && echo $(ibdir) \
	    && mkdir -pv $(init_dir)/$${runname} \
	    && cp -pv $(bashdir)/run-mpgrafic $(installdir)/bin/ \
	    && cd $(init_dir)/$${runname} \
	    && $(installdir)/bin/pwd \
	    && array=($(Hubble)) \
	    && printf "array = $${array}\n" \
	    && H=$${array[$${count}]} \
	    && array=($(OmegaM)) \
	    && OmM=$${array[$${count}]} \
	    && array=($(OmegaL)) \
	    && OmL=$${array[$${count}]} \
	    && USE_HW_THREADS=$(USE_HW_THREADS) \
	          run-mpgrafic $(Lbox) $${Ncroot_digits} $(NCPUS) $(Seed) $${H} $${OmM} $${OmL} \
	    && cd $(badir) \
	    && printf "\nMpgrafic output files are to be found in $(init_dir)/$${runname}.\n\n" \
	    && count=$$(($${count}+1)) \
	    && cd $(curdir) \
	 ; done \
	; done \
	; i_croot=$$(($${i_croot}+1)); printf "init-N-body.mk: run-mpgrafic loop: i_croot=%d\n" $${i_croot}; \
	done

$(mtexdir)/init-N-body.tex: $(paramsfile)
        # This section produces a file with LaTeX macros for the
        # parameters from $(paramsfile). It will be re-run if
        # $(paramsfile) is updated.
	printf "Preparing LaTeX macros for init-N-body.tex...\n"
	printf "%% Automatically produced file.\n\n"
	printf "%% Automatically produced file.\n\n" > $@
        # Single value parameters
	param_list=$$(grep "=" $(paramsfile) | grep -v "^ *#" | egrep -v "Ncroot|flrw_ref|Omega|Hubble|init_pert" | tr -d ' '|tr '\n' ' ')
	printf "param_list=$${param_list}\n\n"
	for param_line in $${param_list}; do
	  param_name_orig=$$(echo $${param_line}|awk -F '=' '{print $$1}')
	  param_name_tex=$$(echo $${param_name_orig}|tr -d '_')
	  param_name=$$(echo $${param_name_orig}|sed -e 's/_/\\\\_/g')
	  param_value=$$(echo $${param_line} | \
	      awk -F '=' '{print $$2}' | sed -e 's/\([eE]\(\|+\|-\)[0-9][0-9]*\)/\\\\times 10^{\1}/' )
	  printf "\\\\newcommand{\\\\$${param_name_tex}name}{{\\\\tt $${param_name}}}\n" >> $@
	  printf "\\\\newcommand{\\\\$${param_name_tex}value}{$${param_value}}\n" >> $@
	done
        #
        # Multiple (in general) value parameters
        #
        # Simulation resolution - final one listed (presumably the highest resolution simulation)
	Ncroot_digits=$(lastword $(Ncroot_inhomog_list))
	Ncroot_letters=($(call powers_two_macro,$(lastword $(Ncroot_inhomog_list))))
        # the parameter name in LaTeX is partly based on the value, but in a simplified way
	param_name_tex=NcrootInhomog$${Ncroot_letters}
	param_value=$${Ncroot_digits}
	printf "\\\\newcommand{\\\\$${param_name_tex}value}{$${param_value}}\n" >> $@
        #
        # Simulation resolution - final one listed (presumably the highest resolution simulation)
	Ncroot_digits=$(lastword $(Ncroot_gevolution_list))
	Ncroot_letters=($(call powers_two_macro,$(lastword $(Ncroot_gevolution_list))))
        # the parameter name in LaTeX is partly based on the value, but in a simplified way
	param_name_tex=NcrootGevolution$${Ncroot_letters}
	param_value=$${Ncroot_digits}
	printf "\\\\newcommand{\\\\$${param_name_tex}value}{$${param_value}}\n" >> $@
        #
        # Perturbation values
	for init_perturb_inhomog in $(init_perturb_inhomog_list); do
	  # the parameter name in LaTeX is partly based on the value, but in a simplified way
	  param_name_tex=$$(echo InitPerturbInhomog$$(echo $${init_perturb_inhomog} | \
	        sed -e 's/-/Minus/' |tr -d '01.') | \
	        sed -e "s/Inhomog\'/InhomogPlus/")
	  param_value=$$(echo $${init_perturb_inhomog})
	  printf "\\\\newcommand{\\\\$${param_name_tex}value}{$${param_value}}\n" >> $@
	done
        #
	count=0
	for flrw in $(flrw_ref); do
	  for param_line in $${param_list}; do
	    param_name_orig=$$(echo $${param_line}|awk -F '=' '{print $$1}')
	    param_name_tex=$$(echo $${param_name_orig}|tr -d '_')$${flrw}
	    param_name=$$(echo $${param_name_orig}_$${flrw}|sed -e 's/[_-]/\\\\_/g')
	    param_value_array=($$(echo $${param_line} | \
	        awk -F '=' '{print $$2}'|sed -e 's/@/ /g'))
	    param_value=$$(echo $${param_value_array[$${count}]} | sed -e 's/\([eE]\(\|+\|-\)[0-9][0-9]*\)/\\\\times 10^{\1}/' )
	  done
	  count=$$(($${count}+1))
	done
        #
        # FLRW model constants
	param_list=$$(grep "=" $(paramsfile)|grep -v "^ *#"|egrep "flrw_ref|Omega|Hubble" | \
	    sed -e 's/ *= */=/' -e "s/ *\'//" | tr ' ' '@' |tr '\n' ' ')
	printf "param_list=$${param_list}\n\n"
	count=0
	for flrw in $(flrw_ref); do
	  for param_line in $${param_list}; do
	    param_name_orig=$$(echo $${param_line}|awk -F '=' '{print $$1}')
	    param_name_tex=$$(echo $${param_name_orig}|tr -d '_')$${flrw}
	    param_name=$$(echo $${param_name_orig}_$${flrw}|sed -e 's/[_-]/\\\\_/g')
	    param_value_array=($$(echo $${param_line} | \
	        awk -F '=' '{print $$2}'|sed -e 's/@/ /g'))
	    param_value=$$(echo $${param_value_array[$${count}]} | sed -e 's/\([eE]\(\|+\|-\)[0-9][0-9]*\)/\\\\times 10^{\1}/' )
	    printf "\\\\newcommand{\\\\$${param_name_tex}name}{{\\\\tt $${param_name}}}\n" >> $@
	    printf "\\\\newcommand{\\\\$${param_name_tex}value}{$${param_value}}\n" >> $@
	  done
	  count=$$(($${count}+1))
	done
        #
	printf "\nThe LaTeX macro file $(mtexdir)/init-N-body.tex was created.\n\n"

clean-init-conditions:
	rm -f $(example_initcond_output_files) $(mpgrafic_sanity_checks)
