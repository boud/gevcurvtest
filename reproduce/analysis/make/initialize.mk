# Project initialization.
#
# Copyright (C) 2018-2023 Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Copyright (C) 2020-2024 Boud Roukema
#
# This Makefile is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This Makefile is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this Makefile.  If not, see <http://www.gnu.org/licenses/>.





# High-level directory definitions
# --------------------------------
#
# Basic directories that are used throughout the project.
#
# Locks are used to make sure that an operation is done in series not in
# parallel (even if Make is run in parallel with the '-j' option). The most
# common case is downloads which are better done in series and not in
# parallel. Also, some programs may not be thread-safe, therefore it will
# be necessary to put a lock on them. This project uses the 'flock' program
# to achieve this.
#
# To help with modularity and clarity of the build directory (not mixing
# software-environment built-products with products built by the analysis),
# it is recommended to put all your analysis outputs in the 'analysis'
# subdirectory of the top-level build directory.
badir=$(BDIR)/analysis
bsdir=$(BDIR)/software

# Derived directories (the locks directory can be shared with software
# which already has this directory.).
texdir      = $(badir)/tex
lockdir     = $(bsdir)/locks
indir       = $(badir)/inputs
prepdir     = $(badir)/prepare
mtexdir     = $(texdir)/macros
installdir  = $(bsdir)/installed
bashdir     = reproduce/analysis/bash
rdir	    = reproduce/analysis/R
pconfdir    = reproduce/analysis/config
softconfdir = reproduce/software/config
ilibpydir   = $(bsdir)/lib/python
isharedir   = $(installdir)/share





# Preparation phase
# -----------------
#
# This Makefile is loaded both for the 'prepare' phase and the 'make'
# phase. But the preparation files should be dealt with differently
# (depending on the phase). In the 'prepare' phase, the main directory
# should be created, and in the 'make' phase, its contents should be
# loaded.
#
# If your project doesn't need any preparation, you can ignore this.
#
# The '-' behind the include commands is used for adding the files only if
# it is possible (they exist). This is necessary because sometimes the user
# will have only '*.conf' or '*.mk' files, or with 'make clean' (where the
# preparation Makefile may call initialize.mk before the main
# 'top-make.mk'). If the '-' is not used, Make will complain about not
# finding these files.
ifeq (x$(project-phase),xprepare)
$(prepdir):; mkdir $@
else
-include $(bsdir)/preparation-done.mk
ifeq (x$(include-prepare-results),xyes)
-include $(prepdir)/*.mk $(prepdir)/*.conf
endif
endif





# TeX build directory
# ------------------
#
# In scenarios where multiple users are working on the project
# simultaneously, they can't all build the final paper together, there will
# be conflicts! It is possible to manage the working on the analysis, so no
# conflict is caused in that phase, but it would be very slow to only let
# one of the project members to build the paper at every instance
# (independent parts of the paper can be added to it independently). To fix
# this problem, when we are in a group setting, we'll use the user's ID to
# create a separate LaTeX build directory for each user.
#
# The same logic applies to the final paper PDF: each user will create a
# separte final PDF (for example 'paper-user1.pdf' and 'paper-user2.pdf')
# and no 'paper.pdf' will be built. This isn't a problem because
# 'initialize.tex' is a .PHONY prerequisite, so the rule to build the final
# paper is always executed (even if it is present and nothing has
# changed). So in terms of over-all efficiency and processing steps, this
# doesn't change anything.
ifeq (x$(GROUP-NAME),x)
texbtopdir  = build
final-paper = paper.pdf
else
user        = $(shell whoami)
texbtopdir  = build-$(user)
final-paper = paper-$(user).pdf
endif
texbdir     = $(texdir)/$(texbtopdir)
tikzdir     = $(texbdir)/tikz





# Original system environment
# ---------------------------
#
# Before defining the local sub-environment here, we'll need to save the
# system's environment for some scenarios (for example after 'clean'ing the
# built programs).
curdir   := $(shell echo $$(pwd))





# High level environment
# ----------------------
#
# We want the full recipe to be executed in one call to the shell. Also we
# want Make to run the specific version of Bash that we have installed
# during './project configure' time.
#
# Regarding the directories, this project builds its major dependencies
# itself and doesn't use the local system's default tools. With these
# environment variables, we are setting it to prefer the software we have
# build here.
#
# 'TEXINPUTS': we have to remove all possible user-specified directories to
# avoid conflicts with existing TeX Live solutions. Later (in 'paper.mk'),
# we are also going to overwrite 'TEXINPUTS' just before 'pdflatex'.
.ONESHELL:
.SHELLFLAGS = -ec
#export TERM=xterm # What is this needed for?
export TEXINPUTS :=
export CCACHE_DISABLE := 1
export PATH := $(installdir)/bin
.SHELLFLAGS = --noprofile --norc -ec
export LDFLAGS := -L$(installdir)/lib
export SHELL := $(installdir)/bin/bash
export CPPFLAGS := -I$(installdir)/include
export LD_LIBRARY_PATH := $(installdir)/lib

# Until we build our own C library, without this, the project's GCC won't
# be able to compile anything if the host C library isn't in a standard
# place: in particular Debian-based operatings sytems. On other systems, it
# will be empty.
export CPATH := $(SYS_CPATH)

# RPATH is automatically written in macOS, so 'DYLD_LIBRARY_PATH' is
# ultimately redundant. But on some systems, even having a single value
# causes crashs (see bug #56682). So we'll just give it no value at all.
export DYLD_LIBRARY_PATH :=

# OpenMPI can depend on an existing 'ssh' or 'rsh' binary. However, because
# of security reasons, its best to not install them, disable any
# remote-shell accesss through this environment variable.
export OMPI_MCA_plm_rsh_agent=false

# Recipe startup script.
export PROJECT_STATUS := make
export BASH_ENV := $(curdir)/reproduce/software/shell/bashrc.sh





# Python enviroment
# -----------------
#
# The main Python environment variable is 'PYTHONPATH'. However, so far we
# have found several other Python-related environment variables on some
# systems which might interfere. To be safe, we are removing all their
# values.
export PYTHONPATH             := $(installdir)/lib/python/site-packages
export PYTHONPATH3            := $(PYTHONPATH)
export _LMFILES_              :=
export PYTHONPATH2            :=
export LOADEDMODULES          :=
export MPI_PYTHON_SITEARCH    :=
export MPI_PYTHON2_SITEARCH   :=
export MPI_PYTHON3_SITEARCH   :=





# High-level level directories
# ----------------------------
#
# These are just the top-level directories for all the separate steps. The
# directories (or possible sub-directories) for individual steps will be
# defined and added within their own Makefiles.
#
# The '.SUFFIXES' rule with no prerequisite is defined to eliminate all the
# default implicit rules. The default implicit rules are to do with
# programming (for example converting '.c' files to '.o' files). The
# problem they cause is when you want to debug the make command with '-d'
# option: they add too many extra checks that make it hard to find what you
# are looking for in the outputs.
.SUFFIXES:
$(lockdir): | $(bsdir); mkdir $@





# Version and distribution tarball definitions.
#
# We need to export 'LD_LIBRARY_PATH' before calling 'git' because we the
# default export of 'LD_LIBRARY_PATH' doesn't take effect at this point
# (only within the recipes). Its also safe to directly use the 'git'
# executable using its absolute location (and not rely on 'PATH' at this
# stage).
project-commit-hash := $(shell \
    if [ -d .git ]; then \
      export LD_LIBRARY_PATH="$(installdir)/lib"; \
      echo $$($(installdir)/bin/git describe --dirty --always --long); \
    else echo NOGIT; fi)

# For some purposes, such as a git bundle, ignore any uncommitted changes:
project-committed-only-hash := $(shell if [ -d .git ]; then \
      export LD_LIBRARY_PATH="$(installdir)/lib"; \
      echo $$($(installdir)/bin/git describe --always --long); \
    else echo NOGIT; fi)
project-package-name := gevcurvtest-$(project-commit-hash)
project-package-name-committed-only := gevcurvtest-$(project-committed-only-hash)
project-package-contents = $(texdir)/$(project-package-name)





# High-level Makefile management
# ------------------------------
#
# About '.PHONY': these are targets that must be built even if a file with
# their name exists.
#
# Only '$(mtexdir)/initialize.tex' corresponds to a file. This is because
# we want to ensure that the file is always built in every run: it contains
# the project version which may change between two separate runs, even when
# no file actually differs.
.PHONY: all clean dist dist-zip dist-lzip texclean distclean \
        $(project-package-contents) $(mtexdir)/initialize.tex

texclean:
	rm -f *.pdf
	rm -rf $(texdir)/build/*
	mkdir $(texdir)/build/tikz # 'tikz' is assumed to already exist.

clean:
#	Delete the top-level PDF file.
	rm -f *.pdf

#	Delete possible LaTeX output in top directory. This can happen when
#	the user has run LaTeX with applications other than maneage. For
#	example, when opening 'paper.tex' file with 'texstudio' and
#	executing 'build'.
	rm -f *.aux *.log *.synctex *.auxlock *.dvi *.out *.run.xml *.bcf

#	Delete all the built outputs except the dependency programs. We'll
#	use Bash's extended options builtin ('shopt') to enable "extended
#	glob" (for listing of files). It allows extended features like
#	ignoring the listing of a file with '!()' that we are using
#	afterwards.
	shopt -s extglob
	rm -rfv $(texdir)/macros/!(dependencies.tex|dependencies-bib.tex|hardware-parameters.tex)
	rm -rfv $(badir)/!(tex) $(texdir)/!(macros|$(texbtopdir))
	rm -rfv $(texdir)/build/!(tikz) $(texdir)/build/tikz/*
	rm -rf $(bsdir)/preparation-done.mk

distclean: clean
#	Without cleaning the Git hooks, we won't be able to easily commit
#	or checkout after this task is done. So we'll remove them first.
	rm -f .git/hooks/post-checkout .git/hooks/pre-commit

#	We'll be deleting the built environent programs and just need the
#	'rm' program. So for this recipe, we'll use the host system's 'rm',
#	not our own.
	$$sys_rm -rf $(BDIR)
	$$sys_rm -f .local .build $(pconfdir)/LOCAL.conf





# Packaging rules
# ---------------
#
# With the rules in this section, you can package the project in a state
# that is ready for building the final PDF with LaTeX. This is useful for
# collaborators who only want to contribute to the text of your project,
# without having to worry about the technicalities of the analysis.
$(project-package-contents): paper.pdf $(texbdir)/paper-full.tex | $(texdir)

#	Set up the output directory, delete it if it exists and remake it
#	to fill with new contents.
	dir=$@
	rm -rf $$dir
	mkdir $$dir

#	Build a small Makefile to help in automatizing the paper building
#	(including the bibliography).
	m=$$dir/Makefile
	echo   "paper.pdf: paper-full.tex"                   > $$m
	printf "\tlatex paper-full && latex paper-full && latex paper-full \\\n" >> $$m
	printf "\t&&dvips paper-full -o paper-tmp.eps \\\n" >> $$m
	printf "\t&&ps2pdf paper-tmp.eps \\\n" >> $$m
	printf "\t&&mv -v paper.pdf $(package-name).pdf\n\n" >> $$m
	echo   ".PHONY: clean"                                   >> $$m
	echo   "clean:"                                          >> $$m
	printf "\trm -f *.aux *.auxlock *.bbl *.bcf\n"           >> $$m
	printf "\trm -f *.blg *.log *.out *.run.xml\n"           >> $$m

#	Copy the top-level contents (see next step for 'paper.tex').
	cp COPYING project README.md README-hacking.md $$dir/

        # Copy the full `paper-full.tex' into place).
	pwd
	cp -pv $(texbdir)/paper-full.tex $$dir/

        # Build the top-level directories.
	mkdir $$dir/reproduce

        # Copy all the necessary `reproduce' and `tex' contents.
	shopt -s extglob
        #cp -r $$(git ls-files tex/src)           $$dir/tex/src
	pwd
	tar -c -f - $$(git ls-files reproduce) | (cd $$dir ; tar -x -f -); cd $${curdir}
	pwd
        #cp -r tex/build/!($(project-package-name)) $$dir/tex/build
	cp -pv $(texbdir)/*.tex $${dir} # TODO: exclude non-exportable files!!
        # WARNING: hardwired per specific projects:
	cp -pv $$(ls $(texbdir)/*.eps |grep -v paper-tmp) $${dir}/ # TODO: exclude non-exportable files!!
	#mkdir -p $${dir}/fig
	#cp -pv $(texbdir)/fig/*.eps $${dir}/fig/ # TODO: exclude non-exportable files!!
        #cp -pv $(texbdir)/*.cl[os] $${dir} # TODO: exclude non-exportable files!!
	cp -pv $(texbdir)/*.cl[os] $${dir} # TODO: exclude non-exportable files!!
	cp -pv $(texbdir)/*.sty $${dir} # TODO: exclude non-exportable files!!

        # Supplementary data files (generally output files; possibly input files too):
	mkdir $${dir}/anc/
	cp -pv $(data-publish-dir)/* $${dir}/anc/

        # Clean up un-necessary/local files: 1) the $(texdir)/build*
        # directories (when building in a group structure, there will be
        # `build-user1', `build-user2' and etc), are just temporary LaTeX
        # build files and don't have any relevant/hand-written files in
        # them. 2) The `LOCAL.conf' and `gnuastro-local.conf' files just
        # have this machine's local settings and are irrelevant for anyone
        # else.
	rm -rf $$dir/tex/build/build*
        #rm $$dir/reproduce/software/config/LOCAL.conf

#	PROJECT SPECIFIC
#	----------------
#	Put any project-specific distribution steps here.

#	----------------



# Package into `.tar.gz' or '.tar.lz'.
dist-arxiv: dist-arXiv

dist-arXiv: $(project-package-contents)
	curdir=$$(pwd)
	cd $(texdir)/$(project-package-name)
	  tar -cv --exclude="./reproduce" --exclude="./project" -f ../$(project-package-name)-arXiv.tar .
	  suffix=gz
	  gzip -f --best ../$(project-package-name)-arXiv.tar
	  cd ..
	rm -rf $(project-package-name)
	cd $$curdir
	mv -v $(texdir)/$(project-package-name)-arXiv.tar.$$suffix ./

dist-journal: paper-full.pdf

#2021-12-23 - obsolete? see the rule for paper-full.pdf in paper.mk
#dist-journal: $(project-package-contents)
#	curdir=$$(pwd)
#	cd $(texdir)/$(project-package-name)
#        # Make things easy for the journal: only list explicitly needed files, no extras.
#	  tar -cv -f ../$(project-package-name)-journal.tar paper-full.tex *.cls *.eps
#	  suffix=gz
#	  gzip -f --best ../$(project-package-name)-journal.tar
#	  cd ..
#	rm -rf $(project-package-name)
#	cd $$curdir
#	mv -v $(texdir)/$(project-package-name)-journal.tar.$$suffix ./


# Package into '.tar.gz' or '.tar.lz'.
dist dist-lzip: $(project-package-contents)
	cd $(texdir)
	tar -cf $(project-package-name).tar $(project-package-name)
	if [ $@ = dist ]; then
	  suffix=gz
	  gzip -f --best $(project-package-name).tar
	elif [ $@ = dist-lzip ]; then
	  suffix=lz
	  lzip -f --best $(project-package-name).tar
	fi
	rm -rf $(project-package-name)
	cd $(curdir)
	mv $(texdir)/$(project-package-name).tar.$$suffix ./

# Package into '.zip'.
dist-zip: $(project-package-contents)
	cd $(texdir)
	zip -q -r $(project-package-name).zip $(project-package-name)
	rm -rf $(project-package-name)
	cd $(curdir)
	mv $(texdir)/$(project-package-name).zip ./

# Package the software tarballs.
dist-software:
	dirname=software-$(project-commit-hash)
	cd $(bsdir)
	if [ -d $$dirname ]; then rm -rf $$dirname; fi
	mkdir $$dirname
	cp -L tarballs/* $$dirname/
	tar -cf $$dirname.tar $$dirname
	gzip -f --best $$dirname.tar
	rm -rf $$dirname
	cd $(curdir)
	mv -v $(bsdir)/$$dirname.tar.gz ./

git-bundle:
	git bundle create $(project-package-name-committed-only)-git.bundle HEAD gevcurvtest
	ls -l $(project-package-name-committed-only)-git.bundle

git-snapshot:
	git archive --format=tar.gz \
	   --prefix=$(project-package-name-committed-only)/ HEAD \
	   > $(project-package-name-committed-only)-snapshot.tar.gz
	ls -l $(project-package-name-committed-only)-snapshot.tar.gz


# Import input data
# -----------------
#
# The list files to be imported (downloaded from a server, or linked from a
# local location), are listed in 'reproduce/analysis/config/INPUTS.conf'
# along with their URLs and verification checksums. In most cases, you will
# not need to edit this rule. Simply follow the instructions at the top of
# 'INPUTS.conf' and set the variables names according to the described
# standards and everything should be fine.
#
# TECHNICAL NOTE on the '$(foreach, n ...)' loop of 'inputdatasets': we are
# using several (relatively complex!) features particular to Make: In GNU
# Make, '.VARIABLES' "... expands to a list of the names of all global
# variables defined so far" (from the "Other Special Variables" section of
# the GNU Make manual). Assuming that the pattern 'INPUT-%-sha256' is only
# used for input files, we find all the variables that contain the input
# file name (the '%' is the filename). Finally, using the
# pattern-substitution function ('patsubst'), we remove the fixed string at
# the start and end of the variable name.
#
# Download lock file: Most systems have a single connection to the
# internet, therefore downloading is inherently done in series. As a
# result, when more than one dataset is necessary for download, if they are
# done in parallel, the speed will be slower than downloading them in
# series. We thus use the 'flock' program to tie/lock the downloading
# process with a file and make sure that only one downloading event is in
# progress at every moment.
$(indir):; mkdir $@
downloadwrapper = $(bashdir)/download-multi-try
inputdatasets := $(foreach i, \
                   $(patsubst INPUT-%-sha256,%, \
                     $(filter INPUT-%-sha256,$(.VARIABLES))) \
                   $(patsubst INPUT-%-fitsdatasum,%, \
                     $(filter INPUT-%-fitsdatasum,$(.VARIABLES))), \
                   $(indir)/$(i))
$(inputdatasets): $(indir)/%: | $(indir) $(lockdir)

#	Starting rule with '@': In case there is a username or password
#	given for the database, we don't want the values to be printed in
#	the terminal as the pipeline is running. We are therefore starting
#	this recipe with an '@' (so Make doesn't print the used
#	commands). To help the user know what is happening (in case they
#	can't tell from the Wget outputs), we'll just start the recipe with
#	a notice on what is being imported.
	@echo "Importing $@"

#	If a username or password has been provided, add them to the WGET
#	command. The two variables are defined in the local configuation
#	file 'reproduce/software/config/LOCAL.conf' that is not under
#	version control. Different servers may use different authentication
#	formats. If the default one doesn't work for your server, comment
#	it and uncomment the one that works. If your serve needs a
#	different kind of authentication format, please add it yourself. In
#	case you need a new format, we encourage you to send the format to
#	us using the link below:
#	https://savannah.nongnu.org/support/?group=reproduce&func=additem
	authopt=""
	if [ x"$(DATABASEAUTHTYPE)" != x ]; then
	  case "$(DATABASEAUTHTYPE)" in

#	    Format: '--user=XXXX --password=YYYY'
	    userpass)
	      if [ x'$(DATABASEUSER)' != x ]; then
	        authopt="--user='$(DATABASEUSER)'"; fi
	      if [ x'$(DATABASEPASS)' != x ]; then
	        authopt="$$authopt --password='$(DATABASEPASS)'"; fi
	      ;;

#	    Format: --post-data 'username=XXXX&password=YYYY'
	    postdata)
	      if [ x'$(DATABASEUSER)' != x ]; then
	        authopt="--post-data 'username=$(DATABASEUSER)"; fi
	      if [ x'$(DATABASEPASS)' != x ]; then
	        authopt="$$authopt""&password=$(DATABASEPASS)'";
	      else authopt="$$authopt'"  # To close the single quote
	      fi
	      ;;

#	    Unrecognized format.
	    *)
	    echo "Maneage: 'DATABASEAUTHTYPE' format not recognized! Please see the description of this variable in 'reproduce/software/config/LOCAL.conf' for the acceptable values."; exit 1;;
	  esac
	fi

#	Download (or make the link to) the input dataset. If the file
#	exists in 'INDIR', it may be a symbolic link to some other place in
#	the filesystem. To avoid too many links when using these files
#	during processing, we'll use 'readlink -f' so the link we make here
#	points to the final file directly (note that 'readlink' is part of
#	GNU Coreutils). If its not a link, the 'readlink' part has no
#	effect.
	unchecked=$@.unchecked
	if [ -f $(INDIR)/$* ]; then
	  ln -fs $$(readlink -f $(INDIR)/$*) $$unchecked
	else
	  touch $(lockdir)/download
	  $(downloadwrapper) "wget $$authopt --no-use-server-timestamps -O" \
	                     $(lockdir)/download "$(INPUT-$*-url)" $$unchecked
	fi

#	Set the checksum related variables.
	if [ x"$(INPUT-$*-sha256)" != x ]; then
	  suffix=sha256
	  sumin=$(INPUT-$*-sha256)
	  verifname="SHA256 checksum"
	  sum=$$(sha256sum $$unchecked | awk '{print $$1}')
	elif [ x"$(INPUT-$*-fitsdatasum)" != x ]; then
	  suffix=fitsdatasum
	  sumin=$(INPUT-$*-fitsdatasum)
	  verifname="FITS standard DATASUM"
	  if [ x"$(INPUT-$*-fitshdu)" = x ]; then hdu=1;
	  else                                    hdu="$(INPUT-$*-fitshdu)"; fi
	  sum=$$(astfits $$unchecked -h$$hdu --datasum  | awk '{print $$1}')
	else
	  echo "$@: checksum for verifyication not recognized!"; exit 1
	fi

#	Verify the input.
	if [ $$sum = $$sumin ]; then
	  mv $$unchecked $@
	  echo "Integrity confirmed, using $@ in this project."

#	Checksums didn't match.
	else

#	  The user has asked to update the checksum in 'INPUTS.conf'.
	  if [ $$sumin = "--auto-replace--" ]; then

#	    Put the updated 'INPUTS.conf' in a temporary file.
	    inputstmp=$@.inputs
	    awk '{if($$1 == "INPUT-$*-'$$suffix'") \
	            $$3="'$$sum'"; print}' \
	            $(pconfdir)/INPUTS.conf > $$inputstmp

#	    Update the INPUTS.conf, but not in parallel (using the
#	    file-lock feature of 'flock').
	    touch $(lockdir)/inputs-update
	    flock $(lockdir)/inputs-update \
	          sh -c "mv $$inputstmp $(pconfdir)/INPUTS.conf"
	    mv $$unchecked $@

#	  Error on non-matching checksums.
	  else
	    echo; echo;
	    echo "Wrong $$verifname for input file '$*':"
	    echo "  File location: $$unchecked"; \
	    echo "  Expected $$verifname:   $$sumin"; \
	    echo "  Calculated $$verifname: $$sum"; \
	    echo; exit 1
	  fi
	fi





# Directory containing to-be-published datasets
# ---------------------------------------------
#
# Its good practice (so you don't forget in the last moment!) to have all
# the plot/figure/table data that you ultimately want to publish in a
# single directory.
#
# There are two types of to-publish data in the project.
#
#  1. Those data that also go into LaTeX (for example to give to LateX's
#     PGFPlots package to create the plot internally) should be under the
#     '$(texdir)' directory (because other LaTeX producers may also need it
#     for example when using './project make dist', or you may want to
#     publish the raw data behind the plots, like:
#     https://zenodo.org/record/4291207/files/tools-per-year.txt). The
#     contents of this directory are also directly taken into the tarball.
#
#  2. The data that aren't included directly in the LaTeX run of the paper,
#     can be seen as supplements. A good place to keep them is under your
#     build-directory.
#
# RECOMMENDATION: don't put the figure/plot/table number in the names of
# your to-be-published datasets! Given them a descriptive/short name that
# would be clear to anyone who has read the paper. Later, in the caption
# (or paper's tex/appendix), you will put links to the dataset on servers
# like Zenodo (see the "Publication checklist" in 'README-hacking.md').
tex-publish-dir = $(texdir)/to-publish
data-publish-dir = $(badir)/data-to-publish
$(tex-publish-dir):; mkdir $@
$(data-publish-dir):; mkdir $@





# Print Copyright statement
# -------------------------
#
# The 'print-general-metadata' can be used to print the general metadata in
# published datasets that are in plain-text format. It should be called
# with make's 'call' function like this (where 'FILENAME' is the name of
# the file it will append this content to):
#
#    $(call print-general-metadata, FILENAME)
#
# See 'reproduce/analysis/make/delete-me.mk' (in the Maneage branch) for a
# real-world usage of this variable.
doi-prefix-url   = https://doi.org
arxiv-prefix-url = https://arxiv.org/abs

# Metadata to write to the beginning of a plain-text data file.
# 1. filename
# 2. [optional] If parameter 2 is set to "new", then the file will be created as a new file.
print-general-metadata = \
	if [ "x$(strip $(2))" = "xnew" ]; then \
	  echo "\# Project title: $(metadata-title)" > $(1); \
	else \
	  echo "\# Project title: $(metadata-title)" >> $(1); \
	fi; \
	echo "\# Git commit (that produced this dataset): $(project-commit-hash)" >> $(1); \
	echo "\# Git repository: $(metadata-git-repository)" >> $(1); \
	if [ x$(metadata-arxiv) != x ]; then \
	  echo "\# Pre-print: $(arxiv-prefix-url)/abs/$(metadata-arxiv)" >> $(1); fi; \
	if [ x$(metadata-doi-journal) != x ]; then \
	  echo "\# DOI (Journal): $(doi-prefix-url)/$(metadata-doi-journal)" >> $(1); fi; \
	if [ x$(metadata-doi-zenodo) != x ]; then \
	echo "\# DOI (Zenodo): $(doi-prefix-url)/$(metadata-doi-zenodo)" >> $(1); fi; \
	echo "\#" >> $(1); \
	echo "\# Copyright (C) $$(date +%Y) $(metadata-copyright-owner)" >> $(1); \
	echo "\# Dataset is available under $(metadata-copyright)." >> $(1); \
	echo "\# License URL: $(metadata-copyright-url)" >> $(1);




# Project initialization results
# ------------------------------
#
# This file will store some basic info about the project that is necessary
# for the final PDF. Since these are not version controlled, it must be
# calculated everytime the project is run. So even though this file
# actually exists, it is also aded as a `.PHONY' target above.
include $(softconfdir)/TARGETS.conf # seems this line got removed in Maneage by 2022-07-02

$(mtexdir)/initialize.tex: | $(mtexdir)
        # Version and title of project. About the starting '@': since these
        # commands are run every time with './project make', it is annoying
        # to print them on the standard output every time. With the '@',
        # make will not print the commands that it runs in this recipe.
	@d=$$(git show -s --format=%aD HEAD | awk '{print $$2, $$3, $$4}')
	echo "\newcommand{\projectdate}{$$d}" > $@
	echo "\newcommand{\projecttitle}{$(metadata-title)}" >> $@
	echo "\newcommand{\projectversion}{$(project-commit-hash)}" >> $@
	echo "\newcommand{\projectzenodoid}{$(metadata-zenodo-id)}" >> $@
	echo "\newcommand{\projectzenodohref}{\href{$(metadata-zenodo-url-base)}{$(metadata-zenodo-id)}}" >> $@
	echo "\newcommand{\projectzenodohrefShowURL}{\href{$(metadata-zenodo-url-base)}{$(metadata-zenodo-url-base)}}" >> $@
	if [ "x${enable_dev_override}" = "xTrue" ]; then \
	   echo "\newcommand{\projectzenodofilesbase}{$(metadata-zenodo-url-base-dev-override)/files}" >> $@; \
	else \
	   echo "\newcommand{\projectzenodofilesbase}{$(metadata-zenodo-url-base)/files}" >> $@;
	fi
	echo "\newcommand{\projectgitrepository}{\url{$(metadata-git-repository)}}" >> $@
	echo "\newcommand{\projectgitrepositoryPlainURL}{$(metadata-git-repository)}" >> $@
	echo "\newcommand{\projectgitrepositoryarchived}{\href{$(metadata-git-repository-archived)}{swh:1:rev:$(metadata-swh-id)}}" >> $@
	echo "\newcommand{\projectgitrepositoryarchivedPlainURL}{$(metadata-git-repository-archived)}" >> $@
	echo "\newcommand{\projectgitrepositoryissues}{\url{$(metadata-git-repository)/issues}}" >> $@

	echo "\newcommand{\projectgevpatchfileSWHhref}{$(metadata-gev-patch-file-swh-href-base)\\\\$(metadata-gev-patch-file-swh-href-full)}" >> $@

#	Calculate the latest Maneage commit used to build this project:
#	  - The project may not have the 'maneage' branch (for example
#	    after cloning from a fork that didn't include it!). In this
#	    case, we'll print a descriptive warning, telling the user what
#	    should be done (reporting the last merged commit and its date
#	    is very useful for the future).
#	  - The '--dirty' option (used in 'project-commit-hash') isn't
#	    applicable to "commit-ishes" (direct quote from Git's error
#	    message!).
	if git log maneage -1 &> /dev/null; then
	  c=$$(git merge-base HEAD maneage)
	  v=$$(git describe --always --long $$c)
	  d=$$(git show -s --format=%aD $$v | awk '{print $$2, $$3, $$4}')
	else
	  echo
	  echo "WARNING: no 'maneage' branch found! Without it, the latest merge of "
	  echo "this project with Maneage can't be reported in the paper (which is bad "
	  echo "for your readers; that includes yourself in a few years). Please run "
	  echo "the commands below to fetch the 'maneage' branch from its own server "
	  echo "and remove this warning (these commands will not affect your project):"
	  echo "   $ git remote add origin-maneage http://git.maneage.org/project.git"
	  echo "   $ git fetch origin-maneage"
	  echo "   $ git branch maneage --track origin-maneage/maneage"
	  echo
	  v="\textcolor{red}{NO-MANEAGE-BRANCH (see printed warning to fix this)}"
	  d="\textcolor{red}{NO-MANEAGE-DATE}"
	fi
	echo "\newcommand{\maneagedate}{$$d}" >> $@
	echo "\newcommand{\maneageversion}{$$v}" >> $@

	# Software target names:
	# Creates macros from foo-bar-3.14.159 such as:
	# \newcommand{\foobarname}{{\sc foo-bar}}
	# \newcommand{\foobarversion}{3.14.159}
	#
	# DISCLAIMER: This is a quick hack that should work in simple
	# cases. Please improve it to make it more robust.
	#
	# TODO: substitution of _ in versions by \_ has not yet been tested.
	for prog_raw in $(top-level-programs); do
          # BUG: this assumes that there are no hyphenated substring
          # TARGETS such as 'foo' as a substring of 'foo-0-bar', since
          # the version number is assumed to start with a digit.
	  prog_filepath=$$(ls -t $(installdir)/version-info/proglib/$${prog_raw}-[0-9]*|head -n 1)
	  prog_nameversion=$$(cat $${prog_filepath}|tr -d '\n')
	  prog_name=$$(echo $${prog_nameversion}|awk '{print $$1}')
	  prog_name_tex=$$(echo $${prog_name}|tr -d '-'|tr -d '_' |sed -e 's/[0-9]/D/g')
	  prog_version=$$(echo $${prog_nameversion}|awk '{print $$2}'| sed -e 's/_/\\\\_/')
	  printf "\\\\newcommand{\\\\$${prog_name_tex}name}{{\\\\sc $${prog_name}}}\n" >> $@
	  printf "\\\\newcommand{\\\\$${prog_name_tex}version}{$${prog_version}}\n" >> $@
	 done

# Rule `create_tex_macro' for creating LaTeX macros for variables
# defined in a given plain text file.
#
# TODO: Searching (grepping) through files is based on unique initial
# substrings. For example, if you have both 'x3 = 1.0' and 'x3_rot = 2.0',
# then this script will create two macros for 'x3', and one for 'x3_rot',
# and LaTeX will complain about a multiply defined macro. So instead,
# write e.g. 'x3_default = 1.0' and 'x3_rot = 2.0', so that the initial strings
# are unique.
#
# TODO: This is a hack that works in particular packages. It should
# be rewritten in a clearer way for beginners, with proper documentation,
# and more robustness against style of parameter files.
#
# Input parameters:
# 1. filename to look through
#
# 2. space-separated list of names of variables (strings)
#
# 3. format in which to write out the value of the variable; current
#    options: str, int, float, float.0, float.1, float.3, float.4,
#    exp, exp.1
#
# 4. a prefix to include in the LaTeX macro name to disambiguate it

create_tex_macro = \
  filename=$(strip $(1)); \
  string_list=$(strip $(2)); \
  format=$(strip $(3)); \
  prefix=$(strip $(4)) \
    && param_list=$$( for param in $${string_list}; do egrep -e "^$${param}" $${filename}|grep -v "^ *\#"|  \
        sed -e "s/\#.*\'//"  -e 's/\([a-zA-Z]\)  *\([0-9]\)/\1=\2/'|sed -e "s/\#.*\'//"| tr -d ' '|tr '\n' ' ' ; done) \
    && printf "param_list=$${param_list}\n\n" \
    && for param_line in $${param_list}; do \
      param_name_orig=$$(echo $${param_line}|awk -F '=' '{print $$1}') \
	&& param_name_tex=$$(printf "$${prefix}_$${param_name_orig}"| \
           sed -e 's/0/z/g' -e 's/1/o/g' -e 's/-/m/g' | \
           sed -e 's/\(.\)/\L\1/g' -e 's/[^_-]*/\u&/g' | \
           tr -d '_'|tr -d '-') \
      && param_name=$$(echo $${param_name_orig}|sed -e 's/_/\\\\_/g') \
      && if [ x$${format} = "xstr" ]; then\
          param_value=$$(echo $${param_line}| \
          awk -F '=' '{printf $$2}' ); \
         fi \
      && if [ x$${format} = "xint" ]; then \
          param_value=$$(echo $${param_line}| \
          awk -F '=' '{printf $$2}' ); \
         fi \
      && if [ x$${format} = "xfloat" ]; then \
          param_value=$$(echo $${param_line}| \
          awk -F '=' '{printf "%.2f" , $$2}' ); \
         fi \
      && if [ x$${format} = "xfloat.0" ]; then \
          param_value=$$(echo $${param_line}| \
          awk -F '=' '{printf "%.0f" , $$2}' ); \
         fi \
      && if [ x$${format} = "xfloat.1" ]; then \
          param_value=$$(echo $${param_line}| \
          awk -F '=' '{printf "%.1f" , $$2}' ); \
         fi \
      && if [ x$${format} = "xfloat.3" ]; then \
          param_value=$$(echo $${param_line}| \
          awk -F '=' '{printf "%.3f" , $$2}' ); \
         fi \
      && if [ x$${format} = "xfloat.4" ]; then \
          param_value=$$(echo $${param_line}| \
          awk -F '=' '{printf "%.4f" , $$2}' ); \
         fi \
      && if [ x$${format} = "xexp" ]; then \
          param_value=$$(echo $${param_line}| \
          awk -F '=' '{printf "%.2e" , $$2}' | sed -e 's/[eE]\(\(\|-\)\|+\)\([0-9][0-9]*\)/\\\\times 10^{\2\3}/' | \
	                  sed -e 's/1\.0*..times//'); \
         fi \
      && if [ x$${format} = "xexp.1" ]; then \
          param_value=$$(echo $${param_line}| \
          awk -F '=' '{printf "%.1e" , $$2}' | sed -e 's/[eE]\(\(\|-\)\|+\)\([0-9][0-9]*\)/\\\\times 10^{\2\3}/' | \
	                  sed -e 's/1\.0*..times//'); \
         fi \
      && printf "\\\\newcommand{\\\\$${param_name_tex}name}{{\\\\tt $${param_name}}}\n" >> $@ \
      && printf "\\\\newcommand{\\\\$${param_name_tex}value}{$${param_value}}\n" >> $@ \
    ; done

## This macro is needed because LaTeX parameter names may not
## contain characters, so it's convenient to avoid digits throughout the
## labelling steps (starting with directory names). The macro is a
## quick hack for powers of two likely to be used in this project for
## simulation sizes. The character strings (in CamelCase) are eight
## characters long.
powers_two_macro = \
   $(subst 32,ThirtTwo,$(subst 64,SixtFour,$(subst 128,HTwEight,$(subst 256,TwFifSix,$(subst 512,FivTwelv,$(1))))))


