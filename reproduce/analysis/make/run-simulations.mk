## Run the simulations

## Include the initial conditions and RAMSES parameters:
include reproduce/analysis/config/Nbody-sim-params.conf

#ibdir = $(bsdir)/installed/bin
iproglibdir = $(installdir)/version-info/proglib
#init_dir = $(badir)/n-body-init # already defined in init-N-body.mk
inhomog_dir = $(badir)/inhomog
#paramsfile = $(pconfdir)/Nbody-sim-params.conf # already defined in init-N-body.mk
## ramses_example_dir is only for *one* sanity check; it doesn't check everything
ramses_example_dir = $(inhomog_dir)/$(word 1, $(flrw_ref))$(word 1, $(subst .,_, $(init_perturb_inhomog_list)))_$(call powers_two_macro,$(word 1, $(Ncroot_inhomog_list)))/output_I
example_sims_output_file = $(ramses_example_dir)/output_00001/part_00001.out00001
ramses_namelist_output_files = $(foreach s, $(flrw_ref), \
            $(foreach t, $(subst .,_, $(init_perturb_inhomog_list)), \
            $(foreach u, $(Ncroot_inhomog_list), \
            $(inhomog_dir)/$(s)$(t)_$(call powers_two_macro,$(u))/output_I/output_00001/namelist.txt)))
done_gevolution = $(badir)/gevolution/done-gevolution
done_gevolution_plot = $(badir)/gevolution/done-gevolution-plot
done_inhomog = $(inhomog_dir)/done-inhomog
done_inhomog_plot = $(inhomog_dir)/done-inhomog-plot
ramses_sanity_checks = $(init_dir)/$(word 1, $(flrw_ref))$(word 1, $(subst .,_, $(init_perturb_inhomog_list)))_$(call powers_two_macro,$(word 1, $(Ncroot_inhomog_list)))/ramses-sanity-check

include ${paramsfile}

RAMSES_part_header_bytes = 200
N_CPUS_RAMSES=$(NCPUS)

# aliases
run-all-simulations: run-ramses run-gevolution

# This is the main 'phony' rule (rule that does not create a file).
#run-ramses: $(done_inhomog) $(example_sims_output_file) $(mtexdir)/run-simulations.tex $(ramses_sanity_checks)

run-ramses: $(done_inhomog) $(ramses_sanity_checks) $(mtexdir)/run-simulations.tex

#$(example_sims_output_file): $(done_inhomog)

#$(ramses_namelist_output_files): $(done_inhomog)

$(done_inhomog): $(paramsfile) $(mpgrafic_sanity_checks) \
              | $(inhomog_dir)
	cp -pv $(bashdir)/run-ramses $(installdir)/bin
	i_croot=0
	Ncroot_letters=($(call powers_two_macro,$(Ncroot_inhomog_list)))
	for Ncroot_digits in $(Ncroot_inhomog_list); do
	 for init_perturb_inhomog in $(init_perturb_inhomog_list); do
	  count=0
	  for flrw in $(flrw_ref); do
	    runname=$${flrw}$$(echo $${init_perturb_inhomog}|tr '.' '_')_$${Ncroot_letters[$${i_croot}]}
	    mkdir -pv $(inhomog_dir)/$${runname}
	    cd $(inhomog_dir)/$${runname}
	    rm -fv ic_*
	    printf "run-simulation.mk: $${PWD}\n"
	    ln -sv $(init_dir)/$${runname}/ic_* .
	    array=($(Hubble))
	    H=$${array[$${count}]}
	    array=($(OmegaM))
	    OmM=$${array[$${count}]}
	    array=($(OmegaL))
	    OmL=$${array[$${count}]}
	    INHOMOG_OUTPARS=$(inhomog_dir)/$${runname}/outputs.$${flrw}
	    rm -f $${INHOMOG_OUTPARS}
	    time_start=$(time_start) time_end=$(time_end) time_step=$(time_step) \
	       LEVELMAX=$(ramses_levelmax) N_CROOT=$${Ncroot_digits} \
	       LBOX=$(Lbox) N_CPUS_RAMSES=$(N_CPUS_RAMSES) \
	       USE_HW_THREADS=$(USE_HW_THREADS) \
	       INHOMOG_DIR=$(inhomog_dir)/$${runname} \
	       INHOMOG_OUTPARS=$${INHOMOG_OUTPARS} \
	       INIT_PERT=$${init_perturb_inhomog} \
	       H0_REF=$${H} OMEGA_M0_REF=$${OmM} OMEGA_LAM0_REF=$${OmL} \
	       run-ramses
	    if [ -s $${INHOMOG_OUTPARS} ]; then
	      printf "\nRAMSES was successfully run for the $${flrw} case. See the new files in $(init_dir)/ and the log file.\n\n"
	    else
	      printf "\nRAMSES was UNsuccessfully run for the $${flrw} case: $${INHOMOG_OUTPARS} was not created. See the new files in $(init_dir)/ and the log file to search for the reason.\n\n"
	      break
	    fi
	    count=$$(($${count}+1))
	  done
	 done
	 i_croot=$$(($${i_croot}+1))
	done
	touch $(done_inhomog)

# Check the byte counts in the particle output files. This sanity check assumes
# that the RAMSES output routine has not changed.

$(ramses_sanity_checks):
	flrw=$(word 1, $(flrw_ref))
	init_perturb_inhomog=$(word 1, $(init_perturb))
	Ncroot_digits=$(word 1, $(Ncroot_inhomog_list))
	Ncroot_letters=($(call powers_two_macro,$(word 1, $(Ncroot_inhomog_list))))
	runname=$${flrw}$$(echo $${init_perturb_inhomog}|tr '.' '_')_$${Ncroot_letters}
	byte_count=$$(wc -c $(ramses_example_dir)/output_00001/part_00*.out00*| \
	  tail -n1 | awk '{print $$1}') \
	&& expected_byte_count=$$(echo $(RAMSES_part_header_bytes) |awk -v N=$${Ncroot_digits} -v ncpu=$(NCPUS) '{print (($$1)*ncpu + 76*N*N*N)}') \
	&& if [ "$${byte_count}" -eq "$${expected_byte_count}" ]; then \
	     printf "The files $(ramses_example_dir)/output_00001/part_00*.out00* have the expected byte count.\n" \
	     && touch $(ramses_example_dir)/ramses-sanity-check \
	;  else \
	     printf "Warning: The files $(init_dir)/$${runname}/output_00001/part_00*.out00* have $${byte_count} bytes but $${expected_byte_count} bytes were expected.\n" \
	     rm -f $(ramses_example_dir)/ramses-sanity-check \
	; fi

run-gevolution: $(done_gevolution) $(done_gevolution_plot)

plot-gevolution: $(done_gevolution_plot)

$(badir)/gevolution:; mkdir $@

$(inhomog_dir):; mkdir $@

$(done_gevolution): | $(badir)/gevolution
	if (grep "experimental-fix" $$(ls -t $(iproglibdir)/gevolution*|head -n1)); then
	  experimental_fix=1;
	else
	  experimental_fix=0;
	fi
	i_croot=0
	Ncroot_letters=($(call powers_two_macro,$(Ncroot_gevolution_list)))
	for Ncroot_digits in $(Ncroot_gevolution_list); do
	 for init_perturb_gevn in $(init_perturb_gevn_list); do
	  count=0
	  for flrw in $(flrw_ref); do
	    runname=$${flrw}$$(echo $${init_perturb_gevn}|tr '.' '_')_$${Ncroot_letters[$${i_croot}]}
	    mkdir -pv $(badir)/gevolution/$${runname}
	    array=($(Hubble))
	    H=$${array[$${count}]}
	    array=($(OmegaM))
	    OmM=$${array[$${count}]}
	    array=($(OmegaL))
	    OmL=$${array[$${count}]}
	    GEVN_OUTPARS=$(badir)/gevolution/$${runname}/outputs.$${flrw}
	    SHARE_DIR=$(isharedir)/gevolution \
	      USE_HW_THREADS=$(USE_HW_THREADS) \
	      FLRW=$${flrw} \
	      GEVN_DIR=$(badir)/gevolution/$${runname} \
	      GEVN_OUTPARS=$${GEVN_OUTPARS} \
	      INIT_PERT=$${init_perturb_gevn} \
	      LBOX=$(Lbox) \
	      N_CROOT=$${Ncroot_digits} \
	      H0_REF=$${H} OMEGA_M0_REF=$${OmM} OMEGA_LAM0_REF=$${OmL} \
	      experimental_fix=$${experimental_fix} \
	      $(bashdir)/run-gevolution
            # Exit if no 'outputs' file has been created
	    if ! [ -s $${GEVN_OUTPARS} ]; then break; fi
	    count=$$(($${count}+1))
	  done
	 done
	 i_croot=$$(($${i_croot}+1))
	done
	touch $(done_gevolution)

$(mtexdir)/run-simulations.tex: $(mtexdir)/inhomog_macros.tex $(mtexdir)/gevolution_macros.tex
	cat $^ > $@

	ls -l $@
	printf "\nThe LaTeX macro file $@ was created.\n\n"

$(mtexdir)/inhomog_macros.tex: $(done_inhomog)
        # This section produces a file with LaTeX macros for some of
        # the parameters in $(ramses_namelist_output_file). It presently
        # selects a small hardwired list of parameters rather than all
        # of them.  This section will be re-run if $(ramses_namelist_output_file) is
        # updated.
	printf "%% Automatically produced file.\n\n"
	printf "%% Automatically produced file.\n\n" > $@
	i_croot=0
	Ncroot_letters=($(call powers_two_macro,$(Ncroot_inhomog_list)))
	for Ncroot_digits in $(call powers_two_macro,$(Ncroot_inhomog_list)); do
	  init_perturb_gevn=$(word 1,$(init_perturb_gevn_list))
	  for flrw in $(flrw_ref); do
	  #namelist_output_file=$${array[$${count}]} # this list includes several loops
	   namelist_output_file=$(inhomog_dir)/$${flrw}$$(echo $${init_perturb_gevn}|tr '.' '_')_$${Ncroot_letters[$${i_croot}]}/output_I/output_00001/namelist.txt
	   param_list=$$(egrep "levelmax|gridsize" $${namelist_output_file}|grep -v "^ *#"|tr -d ' '|tr '\n' ' ')
	   printf "param_list=$${param_list}\n\n"
	   for param_line in $${param_list}; do
	    param_name_orig=$$(echo $${param_line}|awk -F '=' '{print $$1}')
	    param_name_tex="ramsesparam"$$(echo $${param_name_orig}|tr -d '_')$${flrw}$${Ncroot_letters[$${i_croot}]}
	    param_name=$$(echo $${param_name_orig}_$${flrw}_$${Ncroot_letters[$${i_croot}]}|sed -e 's/_/\\\\_/g')
	    param_value=$$(echo $${param_line}| \
	       awk -F '=' '{print $$2}' | sed -e 's/\([eE]\(\|+\|-\)[0-9][0-9]*\)/\\\\times 10^{\1}/' )
	    printf "\\\\newcommand{\\\\$${param_name_tex}name}{{\\\\tt $${param_name}}}\n" >> $@
	    printf "\\\\newcommand{\\\\$${param_name_tex}value}{$${param_value}}\n" >> $@
	   done
	 done
	 i_croot=$$(($${i_croot}+1))
	done

	printf "\nThe LaTeX macro file $@ was created.\n\n"

$(mtexdir)/gevolution_macros.tex: $(done_gevolution)
        # This section produces a file with LaTeX macros for ...
        # This section will be re-run if ... is
        # updated.
	printf "%% Automatically produced file.\n\n"
	printf "%% Automatically produced file.\n\n" > $@

        #
        # Multiple (in general) value parameters
        #
        # Perturbation values
	for init_perturb_gevn in $(init_perturb_gevn_list); do
	  # the parameter name in LaTeX is partly based on the value, but in a simplified way
	  param_name_tex=$$(echo InitPerturbGevn$$(echo $${init_perturb_gevn} | \
	        sed -e 's/-/Minus/' |tr -d '01.') | \
	        sed -e "s/Gevn\'/GevnPlus/")
	  param_value=$$(echo $${init_perturb_gevn})
	  printf "\\\\newcommand{\\\\$${param_name_tex}value}{$${param_value}}\n" >> $@
	done

	count=0
	for Ncroot_letters in $(call powers_two_macro,$(Ncroot_gevolution_list)); do
	 for init_perturb_gevn in $(init_perturb_gevn_list); do
	  for flrw in $(flrw_ref); do
	    runname=$${flrw}$$(echo $${init_perturb_gevn}|tr '.' '_')_$${Ncroot_letters}

	    GEVN_DIR=$(badir)/gevolution/$${runname}
	    param_list=$$(egrep "amplitude" $${GEVN_DIR}/settings.$${flrw}_phi |grep -v "#"|sort -u|tr -d ' '|tr '\n' ' ')
	    printf "param_list=$${param_list}\n\n"
	    for param_line in $${param_list}; do
	      param_name_orig=$$(echo $${param_line}|awk -F '=' '{print $$1}')
	      # We will need an letters-only string to indicate if this is a positive or negative perturbation.
              # We make assumptions on the format, although this could be generalised.
	      param_sign_string=$$(echo $${param_line}| \
	                                   awk -F '=' '{print $$2}' | \
	                                   sed -e 's/^\([0-9]\)/Plus\1/' | \
	                                   sed -e 's/^-/Minus/' |tr -d '[0-9].' )
	      # The parameter name in LaTeX is partly based on the value, but in a simplified way.
              # The sed rule 's/[^_]*/\u&/g' makes camel case, e.g. my_initial_value becomes MyInitialValue .
	      param_name_tex="gevnparam"$$(echo $${param_name_orig}| sed -e 's/[^_]*/\u&/g')$${flrw}$${param_sign_string}$${Ncroot_letters}
	      param_name=$$(echo $${param_name_orig}_$${flrw}_$${runname}|sed -e 's/_/\\\\_/g')
	      param_value=$$(echo $${param_line}| \
	         awk -F '=' '{print $$2}' | sed -e 's/\([eE]\(\|+\|-\)[0-9][0-9]*\)/\\\\times 10^{\1}/' )
	      printf "\\\\newcommand{\\\\$${param_name_tex}name}{{\\\\tt $${param_name}}}\n" >> $@
	      printf "\\\\newcommand{\\\\$${param_name_tex}value}{$${param_value}}\n" >> $@
	    done
	  done
	 done
	done

#	printf "This is a dummy place holder for creating some settings.ini parameter macros... \n" >> $@

	printf "\nThe LaTeX macro file $@ was created.\n\n"

clean-simulations: clean-inhomog clean-gevolution

clean-inhomog:
	rm -fv $(done_inhomog) $(example_sims_output_file)  $(ramses_namelist_output_file)

clean-gevolution:
        # avoid recursive 'rm' to reduce chance of catastrophic error
	shopt -s extglob
	for flrw in $(flrw_ref); do
	  rm -fv $(done_gevolution) $(badir)/gevolution/$${flrw}*/!(settings.ini|*.dat|output_*)
	  rm -fv $(badir)/gevolution/$${flrw}*/*/*
	done

.PHONY: run-all-simulations run-ramses run-gevolution plot-gevolution \
        clean-inhomog clean-gevolution clean-simulations
