# Verify the project outputs before building the paper.
#
# Copyright (C) 2020-2023 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This Makefile is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This Makefile is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this Makefile.  If not, see <http://www.gnu.org/licenses/>.





# Verification functions
# ----------------------
#
# These functions are used by the final rule in this Makefile
verify-print-error-start = \
  echo; \
  echo "VERIFICATION ERROR"; \
  echo "------------------"; \
  echo

verify-print-tips = \
  echo "If you are still developing your project, you can disable"; \
  echo "verification by removing the value of the variable in the"; \
  echo "following file (from the top project source directory):"; \
  echo "    reproduce/analysis/config/verify-outputs.conf"; \
  echo; \
  echo "If this is the final version of the file, you can just copy"; \
  echo "and paste the calculated checksum (above) for the file in"; \
  echo "the following project source file:"; \
  echo "    reproduce/analysis/make/verify.mk"

# Removes following components of a plain-text file, calculates checksum
# and compares with given checksum:
#   - All commented lines (starting with '#') are removed.
#   - All empty lines are removed.
#   - All space-characters in remaining lines are removed (so the width of
#     the printed columns won't invalidate the verification).
#
# It takes three arguments:
#   - First argument: Full address of file to check.
#   - Second argument: Expected checksum of the file to check.
#   - File name to write result.
verify-txt-no-comments-no-space = \
  infile=$(strip $(1)); \
  inchecksum=$(strip $(2)); \
  innobdir=$$(echo $$infile | sed -e's|$(BDIR)/||g'); \
  if ! [ -f "$$infile" ]; then \
    $(call verify-print-error-start); \
    echo "The following file (that should be verified) doesn't exist:"; \
    echo "    $$infile"; \
    echo; exit 1; \
  fi; \
  checksum=$$(sed -e 's/[[:space:]][[:space:]]*//g' \
                  -e 's/\#.*$$//' \
                  -e '/^$$/d' $$infile \
                  | md5sum \
                  | awk '{print $$1}'); \
  if [ x"$$inchecksum" = x"$$checksum" ]; then \
    echo "%% (VERIFIED) $$checksum $$innobdir" >> $(3); \
  else \
    $(call verify-print-error-start); \
    $(call verify-print-tips); \
    echo; \
    echo "Checked file (without empty or commented lines):"; \
    echo "    $$infile"; \
    echo "Expected MD5 checksum:   $$inchecksum"; \
    echo "Calculated MD5 checksum: $$checksum"; \
    echo; exit 1; \
  fi;

# Extracts the most significant numbers from columns 1,2,3,5,6,7 of a
# plain-text file, calculates checksum and compares with given checksum;
# and does the same for columns 4 and 8, but to a different precision;
#
#   - All commented lines (starting with '#') are removed.
#   - All empty lines are removed.
#   - All space and tab characters in remaining lines are removed
#   - The floating points are rounded using 'printf', *hardwired* to a given
#     precision.
#
# It takes three arguments:
#   - First argument: Full address of file to check.
#   - Second argument: Expected checksum of the file to check.
#   - File name to write result.
#
# Disclaimer: this function is a quick hack for a specific project. Please
# generalise it rather than complaining that it's not generic enough. :)
verify-txt-cols123567_48 = \
  infile=$(strip $(1)); \
  inchecksum=$(strip $(2)); \
  innobdir=$$(echo $$infile | sed -e's|$(BDIR)/||g'); \
  if ! [ -f "$$infile" ]; then \
    $(call verify-print-error-start); \
    echo "The following file (that should be verified) doesn't exist:"; \
    echo "    $$infile"; \
    echo; exit 1; \
  fi; \
  checksum=$$(sed -e 's/\s\s*/ /g' \
                  -e 's/\#.*$$//' \
                  -e '/^$$/d' $$infile \
                  | awk '{printf("%.6f %.6f %.6f %.6f %.6f %.6f %.4f %.4f", $$1,$$2,$$3,$$5,$$6,$$7,$$4,$$8)}' \
                  | tr ' ' '\n' \
                  | tr -s ' ' \
                  | md5sum \
                  | awk '{print $$1}'); \
  if [ x"$$inchecksum" = x"$$checksum" ]; then \
    echo "%% (VERIFIED) $$checksum $$innobdir" >> $(3); \
  else \
    $(call verify-print-error-start); \
    $(call verify-print-tips); \
    echo; \
    echo "Checked file (without empty or commented lines):"; \
    echo "    $$infile"; \
    echo "Expected MD5 checksum:   $$inchecksum"; \
    echo "Calculated MD5 checksum: $$checksum"; \
    echo; exit 1; \
  fi;





# Final verification TeX macro (can be empty)
# -------------------------------------------
#
# This is the FINAL analysis step (before going onto the paper. Please use
# this step to verify the contents of the figures/tables used in the paper
# and the LaTeX macros generated from all your processing. It should depend
# on all the LaTeX macro files that are generated (their contents will be
# checked), and any files that go into the tables/figures of the paper
# (generated in various stages of the analysis.
#
# Since each analysis step's data files are already prerequisites of their
# respective TeX macro file, its enough for 'verify.tex' to depend on the
# final TeX macro.
#
# USEFUL TIP: during the early phases of your research (when you are
# developing your analysis, and the values aren't final), you can comment
# the respective lines.
#
# Here is a description of the variables defined here.
#
#   verify-dep: The major step dependencies of 'verify.tex', this includes
#               all the steps that must be finished before it.
#
#   verify-changes: The files whose contents are important. This is
#               essentially the same as 'verify-dep', but it has removed
#               the 'initialize' step (which is information about the
#               pipeline, not the results).
verify-dep = $(filter-out verify paper, $(makesrc))
verify-check0 = $(subst initialize,,$(verify-dep))
verify-check1 = $(subst init-N-body,,$(verify-check0))
verify-check = $(subst run-simulations,,$(verify-check1))
$(mtexdir)/verify.tex: $(foreach s, $(verify-dep), $(mtexdir)/$(s).tex)

#	Make sure that verification is actually requested, the '@' at the
#	start of the recipe is added so Make doesn't print the commands on
#	the standard output because this recipe is run on every call to the
#	project and can be annoying (get mixed in the middle of the
#	analysis outputs or the LaTeX outputs).
	LC_NUMERIC=C
	LANG=C
	@if [ x"$(verify-outputs)" = xyes ]; then

#	  Make sure the temporary output doesn't exist (because we want to
#	  append to it). We are making a temporary output target so if
#	  there is a crash in the middle, Make will not continue. If we
#	  write in the final target progressively, the file will exist, and
#	  its date will be more recent than all prerequisites, so next time
#	  the project is run, Make will continue and ignore the rest of the
#	  checks.
	  rm -f $@.tmp

          # Verify TeX macros (the values that go into the PDF text).
	  for m in $(verify-check); do
	    file=$(mtexdir)/$$m.tex
	    if [ $$m == analyse-plot  ]; then
	        s=aeae4b826fde9e3833457ae69f12279d
	    else echo; echo "'$$m' not recognized."; exit 1
	    fi
	    $(call verify-txt-no-comments-no-space, $$file, $$s, $@.tmp)
	  done

          # Verify the output datasets.
	  for file in $(output_tables_dat); do
	    m=$$(echo $${file}|sed -e 's;$(data-publish-dir)/;;')
            # By default, do not use the "cols123567_48" rule for checksums.
            # The "cols123567_48" rule is useful for the files with highly
            # precise values that are known to vary slightly between
            # runs on the x86_64 and aarch64 architectures.
	    cols123567_48=0
	    if [ x"$$m" = x"flrw_ref_eff_constants.dat" ]; then
	        s=03433ee9d931a44b88e10240a4d99888
	    elif [ x"$$m" = x"inhomog_scale_factors_EdS.dat" ]; then
	        s=3d31d13217b976a2c49bbc4b3b9c346f
	        cols123567_48=1
	    elif [ x"$$m" = x"inhomog_scale_factors_LCDM.dat" ]; then
	        s=c580b120a78f4dd158fb7b90cba28098
	        cols123567_48=1
	    elif [ x"$$m" = x"gevolution_scale_factors_EdS.dat" ]; then
	        s=5c61cd4c226b75df92853d7c404be832
	        cols123567_48=1
	    elif [ x"$$m" = x"gevolution_scale_factors_LCDM.dat" ]; then
	        s=cd185e3767977404110532d789d4fd4f
	        cols123567_48=1
	    elif [ x"$$m" = x"accuracy_parameters.dat" ]; then
	        s=39caec084482f9b7ff7b60d1451b23f8
	    else echo; echo "'$$m' not recognized for verification."; exit 1
	    fi
	    if [ "x$${cols123567_48}" = "x1" ]; then
	      $(call verify-txt-cols123567_48, $$file, $$s, $@.tmp)
	    else
	      $(call verify-txt-no-comments-no-space, $$file, $$s, $@.tmp)
	    fi
	  done

          # Move temporary file to final target.
	  mv $@.tmp $@
	else
	  echo "% Verification was DISABLED!" > $@
	fi
