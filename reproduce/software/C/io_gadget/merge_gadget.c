/*
   merge_gadget - merge gadget output files

   (C) 2019-2020 Marius Peper +co-authors GPL-3+

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

   Other contributions:

   * 2020-05-04 B. Roukema - initialise Npart_read; close unclosed file; free
   memory before closing

*/

#include <stdio.h>
#include <stdlib.h>
#include "read_gadget.h"

struct gadget_header read_header(char *filename)
{
  struct gadget_header header;
  FILE *fp;
  unsigned int BlockBytes;
  int i;
  unsigned int NewBytes;

  printf("open file %s\n",filename);
  fp = fopen(filename, "rb");

  if(NULL == fp){
    printf("Failed to open %s\n",filename);
    exit(1);
  }else{
    printf("Successfully opened %s\n",filename);
  };

  fread(&BlockBytes,sizeof(BlockBytes),1,fp);

  for(i=0; i<6; i++) fread(&header.num_particles[i],sizeof(header.num_particles[i]),1,fp);
  for(i=0; i<6; i++) fread(&header.particle_masses[i],sizeof(header.particle_masses[i]),1,fp);
  fread(&header.scale_factor,sizeof(header.scale_factor),1,fp);
  fread(&header.redshift,sizeof(header.redshift),1,fp);
  fread(&header.flag_sfr,sizeof(header.flag_sfr),1,fp);
  fread(&header.flag_feedback,sizeof(header.flag_feedback),1,fp);
  for(i=0; i<6; i++) fread(&header.num_total_particles[i],sizeof(header.num_total_particles[i]),1,fp);
  fread(&header.flag_cooling,sizeof(header.flag_cooling),1,fp);
  fread(&header.num_files_per_snapshot,sizeof(header.num_files_per_snapshot),1,fp);
  fread(&header.box_size,sizeof(header.box_size),1,fp);
  fread(&header.omega_0,sizeof(header.omega_0),1,fp);
  fread(&header.omega_lambda,sizeof(header.omega_lambda),1,fp);
  fread(&header.h_0,sizeof(header.h_0),1,fp);
  fread(&header.flag_stellarage,sizeof(header.flag_stellarage),1,fp);
  fread(&header.flag_metals,sizeof(header.flag_metals),1,fp);
  for(i=0; i<6; i++) fread(&header.num_total_particles_hw[i],sizeof(header.num_total_particles_hw[i]),1,fp);
  fread(&header.flag_entropy_ics,sizeof(header.flag_entropy_ics),1,fp);
  for(i=0; i<14; i++) header.free[i]=0;

  fseek(fp,sizeof(BlockBytes)+sizeof(header),0);
  fread(&NewBytes,sizeof(NewBytes),1,fp);

  fclose(fp);

  if(BlockBytes!=NewBytes){
    printf("wrong format\n");
    exit(0);
  }

  return header;
}


int main(int argc, char *argv[])
{

  char *filename_base;
  struct gadget_header header;
  int ncpu, icpu;
  int Npart, Nfile, Npart_read=0;
  char suffix[10];
  char filename[100];
  int sub_num_particles[6];

  float *Pos;
  float *Vel;
  long int *id;
  float *mass;
  float *pot;

  FILE *fp;
  unsigned int BlockBytes;

  FILE *outfile;
  unsigned int NewBytes;

  filename_base=argv[1];
  icpu=0;
  sprintf(suffix, "%d", icpu);
  sprintf(filename, "%s%s%s", filename_base,".",suffix);

  header = read_header(filename);
  ncpu = header.num_files_per_snapshot;
  Npart = header.num_total_particles[1];

  Pos=malloc(sizeof(float)*Npart*3);
  Vel=malloc(sizeof(float)*Npart*3);
  id=malloc(sizeof(long int)*Npart);
  mass=malloc(sizeof(float)*Npart);
  pot=malloc(sizeof(float)*Npart);

  printf("ncpu=%i\n",ncpu);
  for(icpu=0;icpu<ncpu;icpu++){

    sprintf(suffix, "%d", icpu);
    sprintf(filename, "%s%s%s", filename_base,".",suffix);

    printf("open file %s to merge\n",filename);
    fp = fopen(filename, "rb");

    fread(&BlockBytes,sizeof(BlockBytes),1,fp);

    for(i=0; i<6; i++) fread(&sub_num_particles[i],sizeof(header.num_particles[i]),1,fp);

    fseek(fp,sizeof(BlockBytes)+sizeof(header),0);
    fread(&NewBytes,sizeof(NewBytes),1,fp);

    if(BlockBytes!=NewBytes){
      printf("wrong format\n");
      exit(0);
    }

    fread(&BlockBytes,sizeof(BlockBytes),1,fp);

    Nfile=sub_num_particles[1];
    /* pos */
    for(i=Npart_read; i < (Npart_read+Nfile); i++){
      fread(&Pos[3*i],sizeof(float),1,fp);
      fread(&Pos[3*i+1],sizeof(float),1,fp);
      fread(&Pos[3*i+2],sizeof(float),1,fp);
      /* printf("%f  %f  %f\n",Pos[3*i],Pos[3*i+1],Pos[3*i+2]); */
    }

    fread(&NewBytes,sizeof(NewBytes),1,fp);

    if(BlockBytes!=NewBytes){
      printf("wrong format\n");
      exit(0);
    }

    fread(&BlockBytes,sizeof(BlockBytes),1,fp);
    /* vel */

    for(i=Npart_read; i < (Npart_read+Nfile); i++){
      fread(&Vel[3*i],sizeof(float),1,fp);
      fread(&Vel[3*i+1],sizeof(float),1,fp);
      fread(&Vel[3*i+2],sizeof(float),1,fp);
      /* printf("%f  %f  %f\n",Pos[3*i],Pos[3*i+1],Pos[3*i+2]); */
    }

    fread(&NewBytes,sizeof(NewBytes),1,fp);

    if(BlockBytes!=NewBytes){
      printf("wrong format\n");
      exit(0);
    }

    fread(&BlockBytes,sizeof(BlockBytes),1,fp);
    /* id */

    for(i=Npart_read; i < (Npart_read+Nfile); i++){
      fread(&id[i],sizeof(long int),1,fp);
    }

    fread(&NewBytes,sizeof(NewBytes),1,fp);

    if(BlockBytes!=NewBytes){
      printf("wrong format\n");
      exit(0);
    }
    fread(&BlockBytes,sizeof(BlockBytes),1,fp);
    /* mass */

    for(i=Npart_read; i < (Npart_read+Nfile); i++){
      fread(&mass[i],sizeof(float),1,fp);
    }

    fread(&NewBytes,sizeof(NewBytes),1,fp);

    if(BlockBytes!=NewBytes){
      printf("wrong format\n");
      exit(0);
    }

    fread(&BlockBytes,sizeof(BlockBytes),1,fp);
    /* pot */
    for(i=Npart_read; i < (Npart_read+Nfile); i++){
      fread(&pot[i],sizeof(float),1,fp);
    }

    fread(&NewBytes,sizeof(NewBytes),1,fp);

    if(BlockBytes!=NewBytes){
      printf("wrong format\n");
      exit(0);
    }

    Npart_read += Nfile;
    fclose(fp);
    if(icpu != 0) for(i=0; i<6; i++) header.num_particles[i] += sub_num_particles[i];
  }

  if(Npart_read!=Npart){
    printf("error while merging the gadget files\n");
    exit(0);
  }

  /* Write the merged gadget file */

  outfile = fopen("out_gadget", "w");

  BlockBytes=sizeof(struct gadget_header);

  fwrite(&BlockBytes, sizeof(BlockBytes),1,outfile);
  fwrite(&header, sizeof(struct gadget_header),1,outfile);
  fwrite(&BlockBytes, sizeof(BlockBytes),1,outfile);

  BlockBytes=3*sizeof(float)*Npart;
  fwrite(&BlockBytes, sizeof(BlockBytes),1,outfile);
  for(i=0;i<Npart;i++){
    fwrite(&Pos[3*i],sizeof(float),1,outfile);
    fwrite(&Pos[3*i+1],sizeof(float),1,outfile);
    fwrite(&Pos[3*i+2],sizeof(float),1,outfile);
  }
  fwrite(&BlockBytes, sizeof(BlockBytes),1,outfile);

  BlockBytes=3*sizeof(float)*Npart;
  fwrite(&BlockBytes, sizeof(BlockBytes),1,outfile);
  for(i=0; i<Npart; i++){
    fwrite(&Vel[3*i],sizeof(float),1,outfile);
    fwrite(&Vel[3*i+1],sizeof(float),1,outfile);
    fwrite(&Vel[3*i+2],sizeof(float),1,outfile);
  }
  fwrite(&BlockBytes, sizeof(BlockBytes),1,outfile);

  BlockBytes=sizeof(long int)*Npart;
  fwrite(&BlockBytes, sizeof(BlockBytes),1,outfile);
  for(i=0; i<Npart; i++){
    fwrite(&id[i],sizeof(long int),1,outfile);
  }
  fwrite(&BlockBytes, sizeof(BlockBytes),1,outfile);

  BlockBytes=sizeof(float)*Npart;
  fwrite(&BlockBytes, sizeof(BlockBytes),1,outfile);
  for(i=0; i<Npart; i++){
    fwrite(&mass[i],sizeof(float),1,outfile);
  }
  fwrite(&BlockBytes, sizeof(BlockBytes),1,outfile);

  BlockBytes=sizeof(float)*Npart;
  fwrite(&BlockBytes, sizeof(BlockBytes),1,outfile);
  for(i=0; i<Npart; i++){
    fwrite(&pot[i],sizeof(float),1,outfile);
  }
  fwrite(&BlockBytes, sizeof(BlockBytes),1,outfile);

  /* Tidy up */

  fclose(outfile);

  free(Pos);
  free(Vel);
  free(id);
  free(mass);
  free(pot);

  return 0;
}
