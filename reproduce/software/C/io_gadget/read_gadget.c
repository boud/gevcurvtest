#include<stdio.h>
#include<stdlib.h>
#include"read_gadget.h" 

int get_particles(char *filename)
{
  struct gadget_header header;
  FILE *fp;
  fp = fopen(filename, "rb");

  unsigned int BlockBytes;

  fread(&BlockBytes,sizeof(BlockBytes),1,fp);
  //printf("sizeof(BlockBytes)=%lu  sizeof(header)=%lu\n",sizeof(BlockBytes),sizeof(header));

  // read in the whole header or just skip it?
  int i;
  for(i=0; i<6; i++) fread(&header.num_particles[i],sizeof(header.num_particles[i]),1,fp);
  int Npart;
  Npart = header.num_particles[1];
  //printf("N =%d\n",Npart);
  fseek(fp,sizeof(BlockBytes)+sizeof(header),0);

  unsigned int NewBytes;
  fread(&NewBytes,sizeof(NewBytes),1,fp);
  fclose(fp);
  
  if(BlockBytes!=NewBytes){
    printf("wrong format\n");
    exit(0);
  }

  return Npart;
}


float *read_gadget(char *filename)
{

  struct gadget_header header;
  FILE *fp;
  fp = fopen(filename, "rb");

  unsigned int BlockBytes;

  fread(&BlockBytes,sizeof(BlockBytes),1,fp);
  //printf("sizeof(BlockBytes)=%lu  sizeof(header)=%lu\n",sizeof(BlockBytes),sizeof(header));

  // read in the whole header or just skip it?
  int i;
  for(i=0; i<6; i++) fread(&header.num_particles[i],sizeof(header.num_particles[i]),1,fp);
  for(i=0; i<6; i++) fread(&header.particle_masses[i],sizeof(header.particle_masses[i]),1,fp);
  fread(&header.scale_factor,sizeof(header.scale_factor),1,fp);
  fread(&header.redshift,sizeof(header.redshift),1,fp);
  fread(&header.flag_sfr,sizeof(header.flag_sfr),1,fp);
  fread(&header.flag_feedback,sizeof(header.flag_feedback),1,fp);
  for(i=0; i<6; i++) fread(&header.num_total_particles[i],sizeof(header.num_total_particles[i]),1,fp);
  fread(&header.flag_cooling,sizeof(header.flag_cooling),1,fp);
  fread(&header.num_files_per_snapshot,sizeof(header.num_files_per_snapshot),1,fp);
  fread(&header.box_size,sizeof(header.box_size),1,fp);
  fread(&header.omega_0,sizeof(header.omega_0),1,fp);
  fread(&header.omega_lambda,sizeof(header.omega_lambda),1,fp);
  fread(&header.h_0,sizeof(header.h_0),1,fp);
  fread(&header.flag_stellarage,sizeof(header.flag_stellarage),1,fp);
  fread(&header.flag_metals,sizeof(header.flag_metals),1,fp);
  for(i=0; i<6; i++) fread(&header.num_total_particles_hw[i],sizeof(header.num_total_particles_hw[i]),1,fp);
  fread(&header.flag_entropy_ics,sizeof(header.flag_entropy_ics),1,fp);
  for(i=0; i<14; i++) header.free[i]=0;
  
  int Npart;
  Npart = header.num_particles[1];
  fseek(fp,sizeof(BlockBytes)+sizeof(header),0);

  unsigned int NewBytes;
  fread(&NewBytes,sizeof(NewBytes),1,fp);

  if(BlockBytes!=NewBytes){
    printf("wrong format\n");
    exit(0);
  }

  fread(&BlockBytes,sizeof(BlockBytes),1,fp);
  float *Pos;
  Pos=malloc(sizeof(float)*Npart*3);

  for(i=0; i<Npart; i++){
    fread(&Pos[3*i],sizeof(float),1,fp);
    fread(&Pos[3*i+1],sizeof(float),1,fp);
    fread(&Pos[3*i+2],sizeof(float),1,fp);
    //printf("%f  %f  %f\n",Pos[3*i],Pos[3*i+1],Pos[3*i+2]);
  }

  fread(&NewBytes,sizeof(NewBytes),1,fp);
  
  if(BlockBytes!=NewBytes){
    printf("wrong format\n");
    exit(0);
  }  

  fread(&BlockBytes,sizeof(BlockBytes),1,fp);
  float *Vel;
  Vel=malloc(sizeof(float)*Npart*3);
  
  for(i=0; i<Npart; i++){
    fread(&Vel[3*i],sizeof(float),1,fp);
    fread(&Vel[3*i+1],sizeof(float),1,fp);
    fread(&Vel[3*i+2],sizeof(float),1,fp);
    //printf("%f  %f  %f\n",Pos[3*i],Pos[3*i+1],Pos[3*i+2]);
  }
  
  fread(&NewBytes,sizeof(NewBytes),1,fp);

  if(BlockBytes!=NewBytes){
    printf("wrong format\n");
    exit(0);
  }

  fread(&BlockBytes,sizeof(BlockBytes),1,fp);
  long int *id;
  id=malloc(sizeof(long int)*Npart);
  
  for(i=0; i<Npart; i++){
    fread(&id[i],sizeof(long int),1,fp);
  }

  fread(&NewBytes,sizeof(NewBytes),1,fp);

  if(BlockBytes!=NewBytes){
    printf("wrong format\n");
    exit(0);
  }

  fread(&BlockBytes,sizeof(BlockBytes),1,fp);
  float *mass;
  mass=malloc(sizeof(float)*Npart);
  
  for(i=0; i<Npart; i++){
    fread(&mass[i],sizeof(float),1,fp);
  }

  fread(&NewBytes,sizeof(NewBytes),1,fp);

  if(BlockBytes!=NewBytes){
    printf("wrong format\n");
    exit(0);
  }

  fread(&BlockBytes,sizeof(BlockBytes),1,fp);
  float *pot;
  pot=malloc(sizeof(float)*Npart);
  
  for(i=0; i<Npart; i++){
    fread(&pot[i],sizeof(float),1,fp);
  }

  fread(&NewBytes,sizeof(NewBytes),1,fp);

  if(BlockBytes!=NewBytes){
    printf("wrong format\n");
    exit(0);
  }

  // hard coded because I want to have the potential in Revolver. More flexible would be nicer
  float *out;
  out=malloc(sizeof(float)*Npart*8);

  for(i=0; i<Npart; i++){
    out[4*i]=Pos[3*i]*header.scale_factor;
    out[4*i+1]=Pos[3*i+1]*header.scale_factor;
    out[4*i+2]=Pos[3*i+2]*header.scale_factor;
    out[4*i+3]=pot[i];
    //printf("Pos_x[%d]=%f   pot[%d]=%f\n",i,out[i],i,out[4*i+3]);
  }

  return out;

}

int main()
{
  char *filename="output";
  int Npart;
  float *Pos;
  int i;

  Npart=get_particles(filename);
  Pos=malloc(sizeof(float)*Npart*4);
  Pos=read_gadget(filename);
  for(i=0; i<Npart; i++){
    printf("%f  %f  %f  %f\n",Pos[4*i],Pos[4*i+1],Pos[4*i+2],Pos[4*i+3]);
  }
  return 0;
}
