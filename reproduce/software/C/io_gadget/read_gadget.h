/*
   read_gadget - header file for merge_gadget

   (C) 2019-2020 Marius Peper GPL-3+

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   See also http://www.gnu.org/licenses/gpl.html

*/

float *read_gadget(char *filename);
int get_particles(char *filename);
struct gadget_header 
{
  unsigned int num_particles[6];
  double   particle_masses[6];
  double   scale_factor;
  double   redshift;
  int  flag_sfr; /* Star formation flag */
  int  flag_feedback; /* Star formation feedback */
  int num_total_particles[6];
  int  flag_cooling; /* Cooling flag */
  int  num_files_per_snapshot;
  double   box_size;
  double   omega_0;
  double   omega_lambda;
  double   h_0;
  int  flag_stellarage;
  int  flag_metals;
  int  num_total_particles_hw[6]; /* High word of total particle number */
  int  flag_entropy_ics;
  int  free[14];
}; 
unsigned int BlockBytes;
int i;
int Npart;
unsigned int NewBytes;
float *Pos;
