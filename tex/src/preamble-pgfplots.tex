%% PGFPlots settings
%% -----------------
%
%% PGFPLOTS is a package in (La)TeX for making plots internally. It fits
%% nicely with the purpose of a reproducible project. But it isn't
%% mandatory. Therefore if you don't need it, just comment/delete the line
%% that includes this file in the top LaTeX source ('paper.tex').
%
%% However, TiKZ and PGFPlots are the recommended way to include figures
%% and plots in your paper. There are two main reasons: 1) it follows the
%% same LaTeX settings as the text of the paper, so the figures will be in
%% the exact same settings (for example font or lines) as the main body of
%% the papers. 2) It doesn't require any extra dependency (it is
%% distributed as part of TeX-live). Adding specific programs/libraries for
%% plots can greatly increase the number of dependencies for the
%% project. For example Python's Matplotlib library is indeed very good,
%% but it requires Python and Numpy. The latter is not easy to build from
%% source, so after a few years, installing the required version can be
%% very frustrating.
%
%% Keeping all BibLaTeX settings in a separate preamble was done in the
%% spirit of modularity to 1) easily managable, 2) If a similar BibLaTeX
%% configuration is necessary in another LaTeX compilation, this file can
%% just be copied there and used.
%
%% PGFPlots uses the (La)TeX TiKZ package to build plots. So we will first
%% do the settings that are necessary in TiKZ, and then go onto the actual
%% PGFPlots package.
%%
%% USAGE:
%
%%  - All plots are made within a 'tikz' directory (that must already be
%%    present in the location LaTeX is run).
%
%%  - Use '\includetikz{XXXX}' to make/use the figure. If a 'makepdf' LaTeX
%%    macro is not defined, then \includetikz will assume a 'XXXX.pdf' file
%%    exists in 'tex/tikz' and simply import it. If 'makepdf' is defined,
%%    then TiKZ/PGFPlot will be called to (possibly) build the plot based
%%    on 'tex/XXXX.tex'. Note that if the contents of 'tex/src/XXXX.tex'
%%    hasn't changed since the last build. TiKZ/PGFPlots won't rebuild the
%%    plot.
%
%% Copyright (C) 2018-2023 Mohammad Akhlaghi <mohammad@akhlaghi.org>
%
%% This file is part of Maneage (https://maneage.org).
%
%% This file is free software: you can redistribute it and/or modify it
%% under the terms of the GNU General Public License as published by the
%% Free Software Foundation, either version 3 of the License, or (at your
%% option) any later version.
%
%% This file is distributed in the hope that it will be useful, but WITHOUT
%% ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
%% FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
%% for more details.
%
%% You should have received a copy of the GNU General Public License along
%% with this file.  If not, see <http://www.gnu.org/licenses/>.




%% Very general TiKZ settings. In particular, to allow faster processing
%% (not having to re-build the plots on every run), we are using the
%% externalization feature of TiKZ. With this option, TiKZ will build every
%% figure independently in a special directory afterwards it will include
%% the built figure in the final file. This has many advantages: 1) if the
%% code for the plot hasn't changed, then the plot won't be re-made (can be
%% slow with detailed plots). 2) You can use the PDFs of the individual
%% plots for other purposes (for example to include in slides) cleanly.
\usepackage{tikz}
\usetikzlibrary{graphs}
\usetikzlibrary{external}
\usetikzlibrary{positioning}
\tikzexternalize
\tikzsetexternalprefix{tikz/}





%% The '\includetikz' can be used to either build the figures using
%% PGFPlots (when '\makepdf' is defined), or use an existing file (when
%% '\makepdf' isn't defined). When making the PDF, it will set the output
%% figure name to be the same as the 'tex/src/XXXX.tex' file that contains
%% the PGFPlots source of the figure. In this way, when using the PDF, it
%% will also have the same name, thus allowing the figures to easily change
%% their place relative to others: figure ordering won't be a problem. This
%% is a problem by default because if an explicit name isn't set at the
%% start, tikz will make images based on their order in the paper.
%
%% This function takes two arguments:
%%     1) The base-name of the LaTeX file with the 'tikzpicture'
%%        environment. As mentioned above, this will also be the name of
%%        the produced figure.
%%     2) The settings to use with 'includegraphics' when an already-built
%%        file should be used.
\newcommand{\includetikz}[2]{%
  \ifdefined\makepdf%
    \tikzsetnextfilename{#1}%
    \input{tex/src/#1.tex}%
  \else
    \includegraphics[#2]{tex/tikz/#1.pdf}
  \fi
}





%% Uncomment the following lines for EPS and PS images. Note that you still
%% have to use the 'pdflatex' executable and also add a '[dvips]' option to
%% graphicx.
%%\tikzset{
%%  external/system call={
%%    rm -f "\image".eps "\image".ps "\image".dvi;
%%    latex \tikzexternalcheckshellescape -halt-on-error
%%          -interaction=batchmode -jobname "\image" "\texsource";
%%    dvips -o "\image".ps "\image".dvi;
%%    ps2eps "\image.ps"
%%  }
%%}




%% Inport and configure PGFPlots.
\usepackage{pgfplots}
\pgfplotsset{compat=newest}
\usepgfplotslibrary{groupplots}
\pgfplotsset{
  axis line style={thick},
  tick style={semithick},
  tick label style = {font=\footnotesize},
  every axis label = {font=\footnotesize},
  legend style = {font=\footnotesize},
  label style = {font=\footnotesize}
  }





%% Nodes in demo graphs
\tikzset{node-terminal/.style={
  rectangle,
  very thick,
  draw=blue!50,
  text centered,
  top color=white,
  minimum size=6mm,
  text width=2.1cm,
  rounded corners=3mm,
  bottom color=blue!20,
  font=\ttfamily}}

\tikzset{node-nonterminal/.style={
  rectangle,
  very thick,
  text centered,
  top color=white,
  text width=2.1cm,
  minimum size=6mm,
  draw=green!50!black!50,
  bottom color=green!80!black!50,
  font=\ttfamily}}

\tikzset{node-nonterminal-thin/.style={
  rectangle,
  thick,
  text centered,
  top color=white,
  text width=2cm,
  minimum size=2mm,
  draw=green!50!black!50,
  bottom color=green!80!black!50,
  font=\ttfamily\scriptsize}}

\tikzset{node-makefile/.style={
  thick,
  rectangle,
  anchor=south,
  minimum width=2.6cm,
  minimum height=5cm,
  draw=green!50!black!50,
  fill=black!10!green!12!white,
}}

\tikzset{node-point/.style={
  circle,
  black!50,
  inner sep=0pt,
  minimum size=0pt,
  fill=white}}

\tikzset{ bbox/.style={
  rectangle,
  minimum width=2.5cm,
  rounded corners=2mm,
  very thick,draw=blue!50,
  top color=white,
  bottom color=blue!20 } }

\tikzset{ rbox/.style={
    rectangle,
    dotted,
    minimum width=2.5cm,
    rounded corners=2mm,
    very thick,draw=red!50!black!50,
    top color=white,
    bottom color=red!50!black!20 } }

\tikzset{ gbox/.style={
    rectangle,
    minimum width=2.5cm,
    very thick,
    draw=green!50!black!50,
    top color=white,
    bottom color=green!50!black!20 } }

\tikzset{ dirbox/.style={
    thick,
    rectangle,
    anchor=north,
    text centered,
    font=\ttfamily,
    minimum width=15cm,
    minimum height=7.5cm,
    draw=brown!50!black!50,
    fill=brown!10!white }}
